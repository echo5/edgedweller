<?php
/**
 * The header
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fusion
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php echo body_atts(); ?>>

	<?php get_template_part('template-parts/header/' . fusion_get_header_style() ); ?>

	<div id="barba-wrapper">
	  <div class="barba-container">
			<div id="content" class="site-content">

<?php
/**
 * The template for displaying a single portfolio item
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package fusion
 */

get_header(); ?>

	<?php
	while ( have_posts() ) : the_post();

		get_template_part( 'template-parts/portfolio/content', fusion_get_portfolio_single_style() );

	endwhile; // End of the loop.
	?>

<?php
get_footer();

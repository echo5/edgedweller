<?php

/**
 * Update global options to avoid theme conflict
 */
function fusion_update_elementor_global_option () {
  update_option('elementor_disable_color_schemes', 'yes');
  update_option('elementor_disable_typography_schemes', 'yes');
}
add_action('after_switch_theme', 'fusion_update_elementor_global_option');

/**
 * Add page settings
 */
if ( ! function_exists( 'fusion_add_elementor_page_settings' ) ) {
    function fusion_add_elementor_page_settings($element, $section_id, $args)
    {

        if ($section_id !== 'section_page_settings')
            return;

        $element->add_control(
            'header_style',
            [
                'label' => __( 'Header Style (from theme options)', 'fusion' ),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => '',
                'options' => [
                    '' => __( 'Default', 'fusion' ),
                    'dark' => __( 'Dark Background', 'fusion' ),
                    'light' => __( 'Light Background', 'fusion' ),
                ],
            ]
        );

        $element->add_control(
            'menu_item_color',
            [
                'label' => __( 'Menu Item Color', 'fusion' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .navbar a' => 'color: {{VALUE}}',
                ],
            ]
        );

        $element->add_control(
            'header_bg_color',
            [
                'label' => __( 'Header Background', 'fusion' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .navbar' => 'background-color: {{VALUE}}',
                ],
            ]
        );

        $element->add_control(
            'particle_background',
            [
                'label' => __( 'Particle Background', 'fusion' ),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'default' => '',
                'label_on' => __( 'On', 'fusion' ),
                'label_off' => __( 'Off', 'fusion' ),
                'return_value' => 'yes',
            ]
        );

        $element->add_control(
            'particle_count',
            [
                'label'   => __( 'Particle Count', 'fusion' ),
                'type'    => \Elementor\Controls_Manager::NUMBER,
                'default' => 20,
                'min'     => 1,
                'max'     => 300,
                'step'    => 5,
                'condition' => [
                    'particle_background' => 'yes',
                ],
            ]
        );

        $element->add_control(
            'particle_colors',
            [
                'label'       => __( 'Particle Colors', 'fusion' ),
                'type'        => \Elementor\Controls_Manager::TEXT,
                'default'     => '',
                'description'     => __( 'Colors separated by commas without spaces.  You can use color names or hex values here.', 'fusion' ),
                'placeholder' => __( 'red,#0080ff', 'fusion' ),
                'condition' => [
                    'particle_background' => 'yes',
                ],
            ]
        );

    }
}
add_action( 'elementor/element/before_section_end', 'fusion_add_elementor_page_settings',10, 3);


/**
 * Load Anywhere Elementor in head to prevent FOUC
 */
function fusion_enqueue_anywhere_elementor_scripts() {
  $args = array( 'post_type' => 'ae_global_templates', 'post_status' => array('publish', 'draft'));
  $loop = new WP_Query( $args );
  while ( $loop->have_posts() ) : $loop->the_post();
      $post_id = get_the_ID();
      $css_file = new \Elementor\Post_CSS_File( $post_id );
      $css_file->enqueue();
  endwhile;
  wp_reset_query();
}
add_action( 'wp_enqueue_scripts', 'fusion_enqueue_anywhere_elementor_scripts', 5 );
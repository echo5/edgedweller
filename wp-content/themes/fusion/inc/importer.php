<?php

/**
 * Demo files
 */
function fusion_import_files() {

	$demos = array(
		'agency' => array(
			'name' => 'Agency',
		),
		'architect' => array(
			'name' => 'Architect',
		),
		'blogger' => array(
			'name' => 'Blogger',
		),
		'creative' => array(
			'name' => 'Creative',
		),
		'design-agency' => array(
			'name' => 'Design Agency',
		),
		'design-company' => array(
			'name' => 'Design Company',
		),
		'digital-marketing' => array(
			'name' => 'Digital Marketing',
		),
		'freelancer' => array(
			'name' => 'Freelancer',
		),
		'gallery' => array(
			'name' => 'Gallery',
		),
		'graphic-designer' => array(
			'name' => 'Graphic Designer',
		),
		'photography' => array(
			'name' => 'Photography',
		),
		'portfolio' => array(
			'name' => 'Portfolio',
		),
		'shop' => array(
			'name' => 'Shop',
		),
		'startup' => array(
			'name' => 'Startup',
		),
		'studio' => array(
			'name' => 'Studio',
		),
	);

	$importer_array = array();

	foreach ($demos as $key=>$demo) {
		$importer_array[] = array(
		    'import_file_name'             => $demo['name'],
		    'local_import_file'            => trailingslashit( get_template_directory() ) . 'assets/demo-files/'. $key . '_content.xml',
		    'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'assets/demo-files/'. $key . '_widgets.wie',
		    'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'assets/demo-files/'. $key . '_options.dat',
		    'import_preview_image_url'     => trailingslashit( get_template_directory_uri() ) . 'assets/demo-files/' . $key . '.jpg',
		);

	}

	return $importer_array;

}
add_filter( 'pt-ocdi/import_files', 'fusion_import_files' );

/**
 * Setup theme after import
 */
function fusion_after_import_setup() {
    $main_menu = get_term_by( 'name', 'Main', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'menu-1' => $main_menu->term_id,
        )
    );

    $front_page_id = get_page_by_title( 'Home' );
    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'fusion_after_import_setup' );
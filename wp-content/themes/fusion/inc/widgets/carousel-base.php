<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

abstract class Widget_Carousel_Base extends Widget_Base {

	public function get_categories() {
		return [ 'fusion-widgets' ];
	}

	public function get_icon() {
		return 'eicon-slider-push';
	}

	public function get_script_depends() {
		return [ 'jquery-slick' ];
	}

	public function is_reload_preview_required() {
		return true;
	}

	protected function carousel_controls() {

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'image',
			]
		);

		$slides_to_show = range( 1, 10 );
		$slides_to_show = array_combine( $slides_to_show, $slides_to_show );

		$this->add_control(
			'slides_to_show',
			[
				'label' => __( 'Slides in View', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => '3',
				'options' => $slides_to_show,
			]
		);

		$this->add_control(
			'slides_to_scroll',
			[
				'label' => __( 'Slides to Scroll', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => '2',
				'options' => $slides_to_show,
				'condition' => [
					'slides_to_show!' => '1',
				],
			]
		);

		$this->add_control(
			'gap',
			[
				'label' => __( 'Gap', 'fusion' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'max' => 100,
					],
				],
				'default' => [
					'size' => 0,
				],
				'show_label' => true,
				'selectors' => [
					'{{WRAPPER}} .slick-list' => 'margin-left: -{{SIZE}}{{UNIT}}; margin-right: -{{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .slick-slide' => 'padding-left: {{SIZE}}{{UNIT}}; padding-right: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'image_stretch',
			[
				'label' => __( 'Image Stretch', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'no',
				'options' => [
					'no' => __( 'No', 'fusion' ),
					'yes' => __( 'Yes', 'fusion' ),
				],
			]
		);

		$this->add_control(
			'stretch_carousel',
			[
				'label' => __( 'Stretch Carousel', 'fusion' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'label_on' => __( 'Yes', 'fusion' ),
				'label_off' => __( 'No', 'fusion' ),
				'return_value' => 'yes',
				'selectors' => [
					'{{WRAPPER}}' => 'position: absolute; width: 100%; height: 100%;',
				],
			]
		);

		$this->add_control(
			'navigation',
			[
				'label' => __( 'Navigation', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'both',
				'options' => [
					'both' => __( 'Arrows and Dots', 'fusion' ),
					'arrows' => __( 'Arrows', 'fusion' ),
					'dots' => __( 'Dots', 'fusion' ),
					'none' => __( 'None', 'fusion' ),
				],
			]
		);

		$this->add_control(
			'show_slide_counter',
			[
				'label' => __( 'Show Slide Counter', 'fusion' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'label_on' => __( 'Show', 'fusion' ),
				'label_off' => __( 'Hide', 'fusion' ),
				'return_value' => 'yes',
			]
		);


		$this->end_controls_section();


		$this->start_controls_section(
			'section_overlay',
			[
				'label' => __( 'Overlay', 'fusion' ),
			]
		);


		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'background_overlay',
				'types' => [ 'none', 'classic', 'gradient', 'video' ],
				'selector' => '{{WRAPPER}} .grid-item .overlay',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_additional_options',
			[
				'label' => __( 'Additional Options', 'fusion' ),
			]
		);

		$this->add_control(
			'pause_on_hover',
			[
				'label' => __( 'Pause on Hover', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'yes',
				'options' => [
					'yes' => __( 'Yes', 'fusion' ),
					'no' => __( 'No', 'fusion' ),
				],
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label' => __( 'Autoplay', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'no',
				'options' => [
					'yes' => __( 'Yes', 'fusion' ),
					'no' => __( 'No', 'fusion' ),
				],
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label' => __( 'Autoplay Speed', 'fusion' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 5000,
			]
		);

		$this->add_control(
			'infinite',
			[
				'label' => __( 'Infinite Loop', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'yes',
				'options' => [
					'yes' => __( 'Yes', 'fusion' ),
					'no' => __( 'No', 'fusion' ),
				],
			]
		);

		$this->add_control(
			'effect',
			[
				'label' => __( 'Effect', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'slide',
				'options' => [
					'slide' => __( 'Slide', 'fusion' ),
					'fade' => __( 'Fade', 'fusion' ),
				],
				'condition' => [
					'slides_to_show' => '1',
				],
			]
		);

		$this->add_control(
			'speed',
			[
				'label' => __( 'Animation Speed', 'fusion' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 500,
			]
		);

		$this->add_control(
			'direction',
			[
				'label' => __( 'Direction', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'ltr',
				'options' => [
					'ltr' => __( 'Left', 'fusion' ),
					'rtl' => __( 'Right', 'fusion' ),
				],
			]
		);

		$this->end_controls_section();


		$this->start_controls_section(
			'section_style_navigation',
			[
				'label' => __( 'Navigation', 'fusion' ),
				'condition' => [
					'navigation' => [ 'arrows', 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_arrows',
			[
				'label' => __( 'Arrows', 'fusion' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_position',
			[
				'label' => __( 'Arrows Position', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'inside',
				'options' => [
					'inside' => __( 'Inside', 'fusion' ),
					'outside' => __( 'Outside', 'fusion' ),
					'outside-bottom-left' => __( 'Outside Bottom Left', 'fusion' ),
					'outside-bottom-right' => __( 'Outside Bottom Right', 'fusion' ),
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_size',
			[
				'label' => __( 'Arrows Size', 'fusion' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 20,
						'max' => 60,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .slick-slider .slick-prev:before, {{WRAPPER}} .slick-slider .slick-next:before' => 'font-size: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_color',
			[
				'label' => __( 'Arrows Color', 'fusion' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .slick-slider .slick-prev:before, {{WRAPPER}} .slick-slider .slick-next:before' => 'color: {{VALUE}};',
					'{{WRAPPER}} .slick-slider .slick-prev .nav-text, {{WRAPPER}} .slick-slider .slick-next .nav-text' => 'color: {{VALUE}};',
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_dots',
			[
				'label' => __( 'Dots', 'fusion' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'dots_position',
			[
				'label' => __( 'Dots Position', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'outside',
				'options' => [
					'outside' => __( 'Outside', 'fusion' ),
					'inside' => __( 'Inside', 'fusion' ),
					'inside-right' => __( 'Inside Right', 'fusion' ),
					'inside-left' => __( 'Inside Left', 'fusion' ),
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		// $this->add_control(
		// 	'dots_size',
		// 	[
		// 		'label' => __( 'Dots Size', 'fusion' ),
		// 		'type' => Controls_Manager::SLIDER,
		// 		'range' => [
		// 			'px' => [
		// 				'min' => 5,
		// 				'max' => 10,
		// 			],
		// 		],
		// 		'selectors' => [
		// 			'{{WRAPPER}} .elementor-image-carousel .slick-dots li button:before' => 'font-size: {{SIZE}}{{UNIT}};',
		// 		],
		// 		'condition' => [
		// 			'navigation' => [ 'dots', 'both' ],
		// 		],
		// 	]
		// );

		$this->add_control(
			'dots_color',
			[
				'label' => __( 'Dots Color', 'fusion' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-slick-slider ul.slick-dots li button:before' => 'background-color: {{VALUE}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->end_controls_section();


	}

	protected function render_carousel($slides, $settings, $post_type = '') {

		if ( $post_type) {
			$query = fusion_get_post_items_by_settings( $settings, $post_type );
			$slides = $query->posts;
		} 

		if ( empty( $slides ) ) {
			return;
		}

		$is_slideshow = '1' === $settings['slides_to_show'];
		$is_rtl = ( 'rtl' === $settings['direction'] );
		$direction = $is_rtl ? 'rtl' : 'ltr';
		$show_dots = ( in_array( $settings['navigation'], [ 'dots', 'both' ] ) );
		$show_arrows = ( in_array( $settings['navigation'], [ 'arrows', 'both' ] ) );

		$slick_options = [
			'slidesToShow' => absint( $settings['slides_to_show'] ),
			'autoplaySpeed' => absint( $settings['autoplay_speed'] ),
			'autoplay' => ( 'yes' === $settings['autoplay'] ),
			'infinite' => ( 'yes' === $settings['infinite'] ),
			'pauseOnHover' => ( 'yes' === $settings['pause_on_hover'] ),
			'speed' => absint( $settings['speed'] ),
			'arrows' => $show_arrows,
			'dots' => $show_dots,
			'rtl' => $is_rtl,
		];

		$carousel_classes = [ 'carousel' ];

		if ( $show_arrows ) {
			$carousel_classes[] = 'slick-arrows-' . $settings['arrows_position'];
		}

		if ( $show_dots ) {
			$carousel_classes[] = 'slick-dots-' . $settings['dots_position'];
		}

		if ( 'yes' === $settings['image_stretch'] ) {
			$carousel_classes[] = 'slick-image-stretch';
		}

		if ( !empty($settings['post_type_style']) ) {
			$carousel_classes[] = $settings['post_type_style'];
		}

		if ( ! $is_slideshow ) {
			$slick_options['slidesToScroll'] = absint( $settings['slides_to_scroll'] );
		} else {
			$slick_options['fade'] = ( 'fade' === $settings['effect'] );
		}
		?>
		<div class="carousel-wrapper elementor-slick-slider <?php echo ($settings['stretch_carousel'] == 'yes') ? 'fit-to-container' : ''; ?>" dir="<?php echo $direction; ?>" data-carousel-settings='<?php echo esc_attr( wp_json_encode( $slick_options ) ); ?>'>
			<div class="<?php echo implode( ' ', $carousel_classes ); ?> slick-slider">
				<?php $n = 0; ?>
				<?php if ($post_type) global $post; ?>
				<?php foreach ($slides as $post): ?>
					<div class="slick-slide">
						<div class="slick-slide-inner">
							<div class="grid-item">
							<?php 
								$GLOBALS['image_size'] = $settings['image_size'];
								if ( $post_type && is_a($post, 'WP_Post') ): 
									setup_postdata($post);
									echo \load_template_part( $post_type . '/' . 'grid' );
								else:
									echo $post;
								endif;
								$n++;
							?>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
				<?php if ($post_type) wp_reset_postdata(); ?>
			</div>
			<?php if ($settings['show_slide_counter'] == 'yes'): ?>
				<div class="carousel-slide-counter"></div>
			<?php endif; ?>
		</div>
		<?php
	}


}

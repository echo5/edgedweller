<?php

/**
 * Post controls
 */

function fusion_add_post_options_to_widget($element, $args) {

    $element->start_controls_section(
        'section_query',
        [
            'label' => __( 'Posts Query', 'fusion' ),
        ]
    );

    $element->add_control(
        'posts_per_page',
        [
            'label' => __( 'Posts per page', 'fusion' ),
            'type' => \Elementor\Controls_Manager::NUMBER,
            'default' => -1,
        ]
    );

    $element->add_control(
        'offset',
        [
            'label' => __( 'Offset', 'fusion' ),
            'type' => \Elementor\Controls_Manager::NUMBER,
            'default' => 0,
        ]
    );

    $element->add_control(
        'category',
        [
            'label' => __( 'Category', 'fusion' ),
            'type' => \Elementor\Controls_Manager::TEXT,
            'description' => 'A list of comma separated category slugs to include (e.g., latest-news,my-blog-category)'
        ]
    );

    $element->add_control(
        'include',
        [
            'label' => __( 'Include Items', 'fusion' ),
            'type' => \Elementor\Controls_Manager::TEXT,
            'description' => 'A list of comma separated IDs of items to include (e.g., 12,36,10)'
        ]
    );

    $element->add_control(
        'exclude',
        [
            'label' => __( 'Exclude Items', 'fusion' ),
            'type' => \Elementor\Controls_Manager::TEXT,
            'description' => 'A list of comma separated IDs of items to exclude (e.g., 12,36,10)'
        ]
    );

    $element->add_control(
        'order',
        [
            'label' => __( 'Order', 'fusion' ),
            'type' => \Elementor\Controls_Manager::SELECT,
            'default' => 'ASC',
            'options' => [
                'ASC' => __( 'Ascending', 'fusion' ),
                'DESC' => __( 'Descending', 'fusion' ),
            ],
        ]
    );

    $element->add_control(
        'orderby',
        [
            'label' => __( 'Order By', 'fusion' ),
            'type' => \Elementor\Controls_Manager::SELECT,
            'default' => '',
            'options' => [
                'menu_order' => __( 'Menu Order', 'fusion' ),
                'date' => __( 'Date', 'fusion' ),
                'title' => __( 'Title', 'fusion' ),
                'modified' => __( 'Date Modified', 'fusion' ),
                'rand' => __( 'Random', 'fusion' ),
            ],
        ]
    );

    $element->add_control(
        'meta_query',
        [
            'label' => __( 'Meta Query', 'fusion' ),
            'type' => \Elementor\Controls_Manager::REPEATER,
            'prevent_empty' => false,
            'default' => array(),
            'fields' => [
                [
                    'name' => 'key',
                    'label' => __( 'Key', 'fusion' ),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'show_label' => true,
                ],
                [
                    'name' => 'value_type',
                    'label' => __( 'Value Type', 'fusion' ),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'default' => 'string',
                    'options' => [
                        'string' => __( 'String', 'fusion' ),
                        'date' => __( 'Date', 'fusion' ),
                        'array' => __( 'Array', 'fusion' ),
                    ],
                    'description' => 'Use string format for default value searching (e.g., "red"), date with a date format and strtotime (e.g., "Y-m-d,next Friday"), or array with a comma separated list (e.g., "red,blue,green").',
                    'show_label' => true,
                ],
                [
                    'name' => 'value',
                    'label' => __( 'Value', 'fusion' ),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'show_label' => true,
                ],
                [
                    'name' => 'compare',
                    'label' => __( 'Compare', 'fusion' ),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'show_label' => true,
                ],
                [
                    'name' => 'type',
                    'label' => __( 'Type', 'fusion' ),
                    'default' => 'CHAR',
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'show_label' => true,
                    'description' => "Possible values are 'NUMERIC', 'BINARY', 'CHAR', 'DATE', 'DATETIME', 'DECIMAL', 'SIGNED', 'TIME', 'UNSIGNED'. Default value is 'CHAR'."
                ],
            ],
        ]
    );

    $element->end_controls_section();

}
add_action( 'elementor/element/portfolio-grid/section_grid/after_section_end', 'fusion_add_post_options_to_widget', 10, 2);
add_action( 'elementor/element/post-grid/section_grid/after_section_end', 'fusion_add_post_options_to_widget', 10, 2);
add_action( 'elementor/element/product-grid/section_grid/after_section_end', 'fusion_add_post_options_to_widget', 10, 2);
add_action( 'elementor/element/portfolio-carousel/section_carousel/after_section_end', 'fusion_add_post_options_to_widget', 10, 2);
add_action( 'elementor/element/post-carousel/section_carousel/after_section_end', 'fusion_add_post_options_to_widget', 10, 2);
add_action( 'elementor/element/custom-post-type-grid/section_grid/after_section_end', 'fusion_add_post_options_to_widget', 10, 2);

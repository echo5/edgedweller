<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Product_Grid extends Widget_Grid_Base {

	public function get_name() {
		return 'product-grid';
	}

	public function get_title() {
		return __( 'Product Grid', 'fusion' );
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_grid',
			[
				'label' => __( 'Grid', 'fusion' ),
			]
		);

		$product_styles = apply_filters( 'luxe_product_grid_styles', array('default' => 'Default'));
		$this->add_control(
			'product_style',
			[
				'label' => __( 'Product Style', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => $product_styles,
			]
		);
		
		$this->grid_controls();

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();
		$settings['post_type_style'] = 'product-style-' . $settings['product_style'];
		$GLOBALS['product_grid_style'] = $settings['product_style'];

		$this->render_grid(null, $settings, 'product');
	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Widget_Product_Grid() );


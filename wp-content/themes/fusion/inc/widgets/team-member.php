<?php
namespace Elementor;

use Elementor\Widget_Base;
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Team_Member extends Widget_Base {

	public function get_name() {
		return 'team_member';
	}

	public function get_title() {
		return __( 'Team Member', 'fusion' );
	}

	public function get_icon() {
		return 'eicon-person';
	}

	public function get_categories() {
		return [ 'fusion-widgets' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_custom_map',
			[
				'label' => __( 'Member', 'fusion' ),
			]
		);

		$this->add_control(
			'image',
			[
				'label' => __( 'Image', 'fusion' ),
				'type' => Controls_Manager::MEDIA,
				'label_block' => true,
			]
		);

		$image_sizes = get_image_sizes();
		$this->add_control(
			'image_size',
			[
				'label' => __( 'Image Size', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'medium',
				'options' => $image_sizes
			]
		);

		$this->add_control(
			'name',
			[
				'label' => __( 'Name', 'fusion' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => 'John Doe',
				'default' => 'John Doe',
				'label_block' => true,
			]
		);

		$this->add_control(
			'title',
			[
				'label' => __( 'Title / Position', 'fusion' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => 'Founder',
				'default' => 'Founder',
				'label_block' => true,
			]
		);

		$this->add_control(
			'info_position',
			[
				'label' => __( 'Text position', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'outside',
				'options' => [
					'left' => __( 'Left', 'fusion' ),
					'bottom' => __( 'Bottom', 'fusion' ),
					'right' => __( 'Right', 'fusion' ),
				],
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();

		$image = '';
		if ($settings['image'])
			$image = wp_get_attachment_image_src($settings['image']['id'], $settings['image_size'])[0];

		$params = [
			'image' => $image,
			'name' => $settings['name'],
			'title' => $settings['title'],
			'info_position' => $settings['info_position']
		];
		$team_member = \load_template_part( 'content-team-member', $params );
		print($team_member);
	}

	protected function _content_template() {}
}
Plugin::instance()->widgets_manager->register_widget_type( new Widget_Team_Member() );

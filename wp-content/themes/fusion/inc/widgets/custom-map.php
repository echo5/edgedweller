<?php
namespace Elementor;

use Elementor\Widget_Base;
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Custom_Map extends Widget_Base {

	public function get_name() {
		return 'custom_map';
	}

	public function get_title() {
		return __( 'Custom Map', 'fusion' );
	}

	public function get_icon() {
		return 'eicon-google-maps';
	}

	public function get_categories() {
		return [ 'fusion-widgets' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_custom_map',
			[
				'label' => __( 'Map', 'fusion' ),
			]
		);

		$default_lat = '33.7490';
		$default_long = '84.3880';
		$this->add_control(
			'lat',
			[
				'label' => __( 'Latitude', 'fusion' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => $default_lat,
				'default' => $default_lat,
				'label_block' => true,
			]
		);

		$this->add_control(
			'long',
			[
				'label' => __( 'Longitude', 'fusion' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => $default_long,
				'default' => $default_long,
				'label_block' => true,
			]
		);

		// $this->add_control(
		// 	'height',
		// 	[
		// 		'label' => __( 'Height', 'fusion' ),
		// 		'type' => Controls_Manager::SLIDER,
		// 		'default' => [
		// 			'size' => 250,
		// 		],
		// 		'range' => [
		// 			'px' => [
		// 				'min' => 100,
		// 				'max' => 600,
		// 			],
		// 		],
		// 	]
		// );


		$this->add_control(
			'marker',
			[
				'label' => __( 'Marker Image', 'fusion' ),
				'type' => Controls_Manager::MEDIA,
				'label_block' => true,
			]
		);

		$this->add_control(
			'zoom',
			[
				'label' => __( 'Zoom Level', 'fusion' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 10,
				],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 20,
					],
				],
			]
		);

		$this->add_control(
			'height',
			[
				'label' => __( 'Height', 'fusion' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 300,
				],
				'range' => [
					'px' => [
						'min' => 40,
						'max' => 1440,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .map-canvas' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
		  'custom_styles',
		  [
		     'label'   => __( 'Custom Styles', 'fusion' ),
		     'type'    => Controls_Manager::CODE,
		     'language' => 'json',
		     'description' => __( 'Insert your Javascript styling array here to customize the map\'s look.  <a href="https://snazzymaps.com/" target="_blank">https://snazzymaps.com</a>', 'fusion')
		  ]
		);

		// $this->add_control(
		// 	'prevent_scroll',
		// 	[
		// 		'label' => __( 'Prevent Scroll', 'fusion' ),
		// 		'type' => Controls_Manager::SWITCHER,
		// 		'default' => 'yes',
		// 		'label_on' => __( 'Yes', 'fusion' ),
		// 		'label_off' => __( 'No', 'fusion' ),
		// 		'selectors' => [
		// 			'{{WRAPPER}} iframe' => 'pointer-events: none;',
		// 		],
		// 	]
		// );

		$this->add_control(
			'view',
			[
				'label' => __( 'View', 'fusion' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();

		if ( empty( $settings['lat'] ) || empty( $settings['long'] ) )
			return;

		if ( 0 === absint( $settings['zoom']['size'] ) )
			$settings['zoom']['size'] = 10;

		$map_options = [
			'lat' => $settings['lat'],
			'long' => $settings['long'],
			'zoom' => $settings['zoom']['size'],
			'styles' => !empty($settings['custom_styles']) ? json_decode($settings['custom_styles']) : null,
			'marker' => $settings['marker']['url'],
		];

		ob_start();

		?>
		<div id="map-canvas" class="map-canvas" data-map-options="<?php echo esc_attr( wp_json_encode( $map_options ) ); ?>" data-map-api-key="AIzaSyAQ1_5mfRGCm6XiZ5pun8IHAGPQXxOqoqQ"></div>
    <?php
    $html = ob_get_clean();
    print($html);
	}

	protected function _content_template() {}
}
Plugin::instance()->widgets_manager->register_widget_type( new Widget_Custom_Map() );

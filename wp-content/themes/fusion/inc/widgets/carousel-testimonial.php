<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Testimonial_Carousel extends Widget_Carousel_Base {

	public function get_name() {
		return 'testimonial-carousel';
	}

	public function get_title() {
		return __( 'Testimonial Carousel', 'fusion' );
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_testimonial_carousel',
			[
				'label' => __( 'Carousel', 'fusion' ),
			]
		);

		$this->add_control(
			'slides',
			[
				'label' => __( 'Slides', 'fusion' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'slide_content' => __( 'Slide #1 content', 'fusion' ),
					],
					[
						'slide_content' => __( 'Slide #2 content', 'fusion' ),
					],
				],
				'fields' => [
					[
						'name' => 'user_name',
						'label' => __( 'User Name', 'fusion' ),
						'type' => Controls_Manager::TEXT,
						'default' => __( 'John Doe', 'fusion' ),
						'show_label' => false,
					],
					[
						'name' => 'user_title',
						'label' => __( 'User Title', 'fusion' ),
						'type' => Controls_Manager::TEXT,
						'default' => __( 'Founder & Ceo', 'fusion' ),
						'show_label' => false,
					],
					[
						'name' => 'user_image',
						'label' => __( 'User Image', 'fusion' ),
						'type' => Controls_Manager::MEDIA,
						'default' => [
							'url' => Utils::get_placeholder_image_src(),
						],
						'show_label' => false,
					],
					[
						'name' => 'content',
						'label' => __( 'Content', 'fusion' ),
						'type' => Controls_Manager::WYSIWYG,
						'default' => __( 'Slide Content', 'fusion' ),
						'show_label' => false,
					],
				],
				'title_field' => '{{{ user_name }}}',
			]
		);

		$this->add_control(
			'alignment',
			[
				'label' => __( 'Alignment', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'center' => __( 'Center', 'fusion' ),
					'left' => __( 'Left', 'fusion' ),
					'right' => __( 'Right', 'fusion' ),
				],
			]
		);

		$this->add_control(
			'view',
			[
				'label' => __( 'View', 'fusion' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->carousel_controls();

	}

	protected function render() {

		$settings = $this->get_settings();
		$slides = [];
		$content_slides = $settings['slides'];

		if (empty($content_slides))
			return;

		foreach ($content_slides as $slide) {
			$params = [
				'user_name' => $slide['user_name'],
				'user_title' => $slide['user_title'],
				'user_image' => $slide['user_image'],
				'content' => $slide['content'],
				'alignment' => $settings['alignment']
			];
			$slides[] = \load_template_part( 'content-testimonial', $params );
		}

		$this->render_carousel($slides, $settings);

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Widget_Testimonial_Carousel() );
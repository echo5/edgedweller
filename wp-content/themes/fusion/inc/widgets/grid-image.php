<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Image_Grid extends Widget_Grid_Base {

	public function get_name() {
		return 'image-grid';
	}

	public function get_title() {
		return __( 'Image Grid', 'fusion' );
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_grid',
			[
				'label' => __( 'Grid', 'fusion' ),
			]
		);

		$this->add_control(
			'grid_images',
			[
				'label' => __( 'Images', 'fusion' ),
				'type' => Controls_Manager::GALLERY,
			]
		);
		
		$this->grid_controls();

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();
		$items = [];
		$images = $settings['grid_images'];

		if (empty($images))
			return;

		foreach ($images as $image) {
			$items[] = '<article><div class="featured"><img src="' . $image['url'] . '"></div></article>';
		}

		$this->render_grid($items, $settings);
	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Widget_Image_Grid() );


<?php

/**
 * Animation controls
 */
function fusion_add_animation_options_to_widget($element, $section_id, $args) {

    if ( $section_id == 'section_advanced' || '_section_style' === $section_id ) {

        $element->start_controls_section(
            '_section_javascript_animation',
            [
                'label' => __( 'Javascript Animation', 'fusion' ),
                'tab' => \Elementor\Controls_Manager::TAB_ADVANCED,
            ]
        );

        $default_animations = array(
            '' => __( 'None', 'fusion' ),
        );
        $animation_options = apply_filters( 'luxe_animation_options', $default_animations );

        $element->add_control(
            'javascript_animation',
            [
                'type' => \Elementor\Controls_Manager::SELECT,
                'label' => __( 'Animation', 'fusion' ),
                 'default' => 'static',
                 'options' => $animation_options,
            ]
        );

        $element->add_control(
          'javascript_animation_duration',
          [
             'label'   => __( 'Duration', 'fusion' ),
             'type'    => \Elementor\Controls_Manager::NUMBER,
             'default' => 900,
             'min'     => 100,
             'max'     => 10000,
             'step'    => 100,
          ]
        );

        $element->add_control(
          'javascript_animation_delay',
          [
             'label'   => __( 'Delay', 'fusion' ),
             'type'    => \Elementor\Controls_Manager::NUMBER,
             'default' => 0,
             'min'     => 0,
             'max'     => 10000,
             'step'    => 100,
          ]
        );

        $element->add_control(
          'javascript_animation_loop',
          [
             'label'   => __( 'Loop', 'fusion' ),
             'type'    => \Elementor\Controls_Manager::NUMBER,
             'default' => 0,
             'min'     => 0,
             'max'     => 100,
             'step'    => 1,
          ]
        );


        $element->end_controls_section();

    }

}
add_action( 'elementor/element/after_section_end', 'fusion_add_animation_options_to_widget', 10, 3);

/**
 * Animation frontend
 */
function fusion_add_animation_attributes_to_elements(\Elementor\Element_Base $element) {
    if ( $element->get_settings( 'javascript_animation' ) == null ||  $element->get_settings( 'javascript_animation' ) == '' || $element->get_settings( 'javascript_animation' ) == 'static') {
        return;
    }

    $element->add_render_attribute( '_wrapper', [
        'class' => 'has-animation',
        'data-animation' => $element->get_settings( 'javascript_animation' ),
        'data-animation-delay' => $element->get_settings( 'javascript_animation_delay' ),
        'data-animation-duration' => $element->get_settings( 'javascript_animation_duration' ),
        'data-animation-loop' => $element->get_settings( 'javascript_animation_loop' ),
    ] );
}
add_action( 'elementor/frontend/element/before_render', 'fusion_add_animation_attributes_to_elements');
add_action( 'elementor/frontend/section/before_render', 'fusion_add_animation_attributes_to_elements');
add_action( 'elementor/frontend/column/before_render', 'fusion_add_animation_attributes_to_elements');
add_action( 'elementor/frontend/widget/before_render', 'fusion_add_animation_attributes_to_elements');
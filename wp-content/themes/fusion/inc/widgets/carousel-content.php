<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Content_Carousel extends Widget_Carousel_Base {

	public function get_name() {
		return 'content-carousel';
	}

	public function get_title() {
		return __( 'Content Carousel', 'fusion' );
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_content_carousel',
			[
				'label' => __( 'Carousel', 'fusion' ),
			]
		);

		$this->add_control(
			'slides',
			[
				'label' => __( 'Slides', 'fusion' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'slide_content' => __( 'Slide #1 content', 'fusion' ),
					],
					[
						'slide_content' => __( 'Slide #2 content', 'fusion' ),
					],
				],
				'fields' => [
					[
						'name' => 'slide_content',
						'label' => __( 'Content', 'fusion' ),
						'type' => Controls_Manager::WYSIWYG,
						'default' => __( 'Slide Content', 'fusion' ),
						'show_label' => false,
					],
				],
				// 'title_field' => '{{{ tab_title }}}',
			]
		);

		$this->carousel_controls();

	}

	protected function render() {

		$settings = $this->get_settings();
		$slides = [];
		$content_slides = $settings['slides'];

		if (empty($content_slides))
			return;

		foreach ($content_slides as $slide) {
			$slides[] = $slide['slide_content'];
		}

		$this->render_carousel($slides, $settings);

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Widget_Content_Carousel() );
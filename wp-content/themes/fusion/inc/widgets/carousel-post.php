<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Post_Carousel extends Widget_Carousel_Base {

	public function get_name() {
		return 'post-carousel';
	}

	public function get_title() {
		return __( 'Post Carousel', 'fusion' );
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_carousel',
			[
				'label' => __( 'Carousel', 'fusion' ),
			]
		);

		$post_styles = apply_filters( 'luxe_post_styles', array('default' => 'Default'));
		$this->add_control(
			'portfolio_style',
			[
				'label' => __( 'Post Style', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => $post_styles,
			]
		);

		$this->carousel_controls();

	}

	protected function render() {
		$settings = $this->get_settings();
		$GLOBALS['post_grid_style'] = $settings['post_style'];

		$this->render_carousel(null, $settings, 'post');
	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Widget_Post_Carousel() );
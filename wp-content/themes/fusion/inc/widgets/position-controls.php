<?php

/**
 * Position controls
 */

function fusion_add_position_options_to_widget($element, $section_id, $args) {

    if ( $section_id == 'section_advanced' || '_section_style' === $section_id ) {

        $element->start_controls_section(
            '_section_position',
            [
                'label' => __( 'Position', 'fusion' ),
                'tab' => \Elementor\Controls_Manager::TAB_ADVANCED,
            ]
        );

        // $element->add_control(
        //     'rotate_item',
        //     [
        //         'label' => __( 'Move Element', 'fusion' ),
        //         'type' => \Elementor\Controls_Manager::SWITCHER,
        //         'default' => '',
        //         'label_on' => __( 'On', 'fusion' ),
        //         'label_off' => __( 'Off', 'fusion' ),
        //         'return_value' => 'yes',
        //     ]
        // );

        $element->add_control(
            'element_position',
            [
                'type' => \Elementor\Controls_Manager::SELECT,
                'label' => __( 'Position', 'fusion' ),
                 'default' => '',
                 'options' => [
                    ''  => __( 'Default', 'fusion' ),
                    'static'  => __( 'Static', 'fusion' ),
                    'relative' => __( 'Relative', 'fusion' ),
                    'absolute' => __( 'Absolute', 'fusion' ),
                    'fixed' => __( 'Fixed', 'fusion' ),
                 ],
                 'selectors' => [ // You can use the selected value in an auto-generated css rule.
                    '{{WRAPPER}}' => 'position: {{VALUE}}',
                 ],
            ]
        );

        $element->add_control(
            'element_left',
            [
                'label' => __( 'Left', 'fusion' ),
                'type' => \Elementor\Controls_Manager::SLIDER,
                'default' => [
                    'size' => 0,
                ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}' => 'left: {{SIZE}}{{UNIT}} !important;',
                ],
                'condition' => [
                    'element_position!' => array('','static'),
                ],
            ]
        );
        $element->add_control(
            'element_top',
            [
                'label' => __( 'Top', 'fusion' ),
                'type' => \Elementor\Controls_Manager::SLIDER,
                'default' => [
                    'size' => 0,
                ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}' => 'top: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'element_position!' => array('','static'),
                ],
            ]
        );

        $element->add_control(
            'element_rotate',
            [
                'label' => __( 'Rotate Item', 'fusion' ),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'default' => '',
                'label_on' => __( 'On', 'fusion' ),
                'label_off' => __( 'Off', 'fusion' ),
                'return_value' => 'yes',
            ]
        );

        $element->add_control(
            'element_rotate_degrees',
            [
                'label' => __( 'Rotate', 'fusion' ),
                'type' => \Elementor\Controls_Manager::SLIDER,
                'default' => [
                    'size' => 0,
                ],
                'range' => [
                    'px' => [
                        'min' => -180,
                        'max' => 180,
                    ],
                ],
                'size_units' => [ 'px' ],
                'selectors' => [
                    '{{WRAPPER}}' => 'transform: rotate({{SIZE}}deg);',
                ],
                'condition' => [
                    'element_rotate' => 'yes',
                ],
            ]
        );

        $element->add_control(
            'element_transform_origin',
            [
                'label' => __( 'Transform Origin', 'fusion' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => '50% 50%',
                'selectors' => [
                    '{{WRAPPER}}' => 'transform-origin: {{VALUE}};',
                ],
                'condition' => [
                    'element_rotate' => 'yes',
                ],
            ]
        );

        // $element->add_control(
        //     'z_index',
        //     [
        //         'label' => __( 'Z-Index', 'fusion' ),
        //         'type' => \Elementor\Controls_Manager::NUMBER,
        //         'default' => '',
        //         'selectors' => [
        //             '{{WRAPPER}}' => 'z-index: {{VALUE}};',
        //         ],
        //         'condition' => [
        //             'element_position!' => array('','static'),
        //         ],
        //     ]
        // );

        $element->add_control(
            'overflow',
            [
                'label' => __( 'Overflow', 'fusion' ),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => '',
                'options' => [
                   ''  => __( 'Default', 'fusion' ),
                   'hidden' => __( 'Hidden', 'fusion' ),
                   'visible' => __( 'Visible', 'fusion' ),
                ],
                'selectors' => [
                    '{{WRAPPER}}' => 'overflow: {{VALUE}};',
                ],
            ]
        );

        $element->end_controls_section();

    }

}
add_action( 'elementor/element/after_section_end', 'fusion_add_position_options_to_widget', 10, 3);

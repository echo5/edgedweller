<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Portfolio_Grid extends Widget_Grid_Base {

	public function get_name() {
		return 'portfolio-grid';
	}

	public function get_title() {
		return __( 'Portfolio Grid', 'fusion' );
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_grid',
			[
				'label' => __( 'Grid', 'fusion' ),
			]
		);

		$portfolio_styles = apply_filters( 'luxe_portfolio_grid_styles', array('default' => 'Default'));
		$this->add_control(
			'portfolio_style',
			[
				'label' => __( 'Portfolio Style', 'fusion' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => $portfolio_styles,
			]
		);
		
		$this->grid_controls();

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();
		$settings['post_type_style'] = 'portfolio-style-' . $settings['portfolio_style'];
		$GLOBALS['portfolio_grid_style'] = $settings['portfolio_style'];

		$this->render_grid(null, $settings, 'portfolio');
	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Widget_Portfolio_Grid() );


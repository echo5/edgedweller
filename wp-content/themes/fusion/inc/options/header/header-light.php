<?php

LuxeOption::add_section( 'header_light', array(
    'title'          => esc_attr__( 'Light Background Header', 'fusion' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'header'
) );

LuxeOption::add_field( 'luxe_options', array(
    'settings' => 'logo_light',
    'label'    => esc_attr__( 'Light Background Logo', 'fusion' ),
    'section'  => 'header_light',
    'type'     => 'image',
    'priority' => 10,
    'default'  => '',
    // 'transport'   => 'postMessage',
    // 'js_vars'     => array(
    //     array(
    //         'element'  => 'header .navbar-brand',
    //         'function' => 'customize_preview_js',
    //     ),
    // ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'header_light_bg_color',
    'label'       => esc_attr__( 'Header Background Color', 'fusion' ),
    'description' => esc_attr__( 'Set the color of your header\'s background.', 'fusion' ),
    'help'        => esc_attr__( 'This is a tooltip', 'fusion' ),
    'section'     => 'header_light',
    'default'     => '#fff',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.header-light-active .navbar, .header-light-active .navbar-inner',
            'property' => 'background-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.header-light-active .navbar, .header-light-active .navbar-inner',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'header_light_typography_color',
    'label'       => esc_attr__( 'Light Header Font Color', 'fusion' ),
    'description' => esc_attr__( 'Set the color of your light header font.', 'fusion' ),
    'section'     => 'header_light',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.header-light-active .navbar, .header-light-active .navbar a, .header-light-active .navbar i.icon',
            'property' => 'color',
        ),
        array(
            'element'  => '.header-light-active .nav-btn .nav-icon span',
            'property' => 'border-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.header-light-active .navbar, .header-light-active .navbar a, .header-light-active .navbar i.icon',
            'function' => 'css',
            'property' => 'color',
        ),
        array(
            'element'  => '.header-light-active .nav-btn .nav-icon span',
            'function' => 'css',
            'property' => 'bordercolor',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'header_light_typography_color_hover',
    'label'       => esc_attr__( 'Light Header Font Hover Color', 'fusion' ),
    'description' => esc_attr__( 'Set the color of your light header font when hovered.', 'fusion' ),
    'section'     => 'header_light',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.header-light-active .navbar a:hover, .header-light-active .navbar .nav-btn:hover, .header-light-active .nav-primary li.current-menu-item a, .header-light-active .navbar a:hover i.icon',
            'property' => 'color',
        ),
        array(
            'element'  => '.header-light-active .nav-btn:hover .nav-icon span',
            'property' => 'border-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'dimension',
    'settings'    => 'header_light_border_width',
    'label'       => esc_attr__( 'Light Header Border Width', 'fusion' ),
    'description' => esc_attr__( 'The border between your light header and content.', 'fusion' ),
    'section'     => 'header_light',
    'default'     => '0px',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.header-light-active .navbar',
            'property' => 'border-width',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.header-light-active .navbar',
            'function' => 'css',
            'property' => 'border-width',
        ),
    ),
    'choices' => array(
        'units' => array( 'px' )
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'header_light_border_color',
    'label'       => esc_attr__( 'Light Header Border Color', 'fusion' ),
    'description' => esc_attr__( 'Set the color of your light header font when hovered.', 'fusion' ),
    'section'     => 'header_light',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.header-light-active .navbar',
            'property' => 'border-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.header-light-active .navbar',
            'function' => 'css',
            'property' => 'border-color',
        ),
    ),
) );

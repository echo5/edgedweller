<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package fusion
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function fusion_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds post index/grid and single styles
	if ( is_singular('post') ) {
		$classes[] = 'single-post-style-' . fusion_get_post_single_style();
	}
	if ( is_singular('portfolio') ) {
		$classes[] = 'single-portfolio-style-' . fusion_get_portfolio_single_style();
	}
	if ( is_home() ) {
		$classes[] = fusion_get_post_grid_style();
	}

	// Header
	$classes[] = 'header-style-' . fusion_get_header_style();
	//Blogs/archives/singles
	if (is_home() || is_archive() || is_single()) {
		$header_style = get_theme_mod( 'blog_header_scheme', 'light' );
	}
	// Single pages
	elseif ($page_id = get_the_ID() && fusion_is_elementor_active()) {
		$elementor_page = \Elementor\PageSettings\Manager::get_page( get_the_ID() ); 
		$page_header_style = $elementor_page->get_settings( 'header_style' );
		if (isset($page_header_style) && $page_header_style) {
			$header_style = $page_header_style;
		}
	}
	// Default
 	if (!isset($header_style)) {
		$header_style = get_theme_mod( 'header_scheme', 'light' );
	}
	$classes[] = 'header-' . $header_style . '-active';

	return $classes;
}
add_filter( 'body_class', 'fusion_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function fusion_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'fusion_pingback_header' );

/**
 * Insert user custom js
 */
function fusion_add_user_custom_js() {
    $js = get_theme_mod( 'custom_js', false );
    if ($js) {
        echo '<script typed="text/javascript">' . $js . '</script>';
    }
}
add_action('wp_head', 'fusion_add_user_custom_js', 110);

/**
 * Insert user custom scripts in head
 */
function fusion_add_user_custom_head_scripts() {
    $scripts = get_theme_mod( 'custom_head_scripts', false );
    if ($scripts) {
        $allowed_html = array(
            'script' => array(
                'src' => array(),
                'type' => array(),
            ),
            'link' => array(
                'rel' => array(),
                'type' => array(),
                'href' => array(),
            ),
        );
        echo wp_kses($scripts, $allowed_html);
    }
}
add_action('wp_head', 'fusion_add_user_custom_head_scripts');

/**
 * Add custom user font options
 */
function fusion_add_custom_fonts( $kirki_fonts ) {
    $user_fonts = array();
    $custom_fonts = get_theme_mod('custom_fonts', false);
    if (is_array($custom_fonts)) {
      foreach ($custom_fonts as $font) {
        $user_fonts['"' . $font['font_name'] . '"'] = array(
            'label' => $font['font_name'],
            'stack'  => $font['font_name'] . ',Helvetica,Arial,sans-serif',
        );
      }
    }
    $fonts = array_merge($user_fonts, $kirki_fonts);
    return $fonts;
}
add_filter( 'kirki/fonts/standard_fonts', 'fusion_add_custom_fonts' );

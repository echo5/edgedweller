<?php

namespace Elementor;

if (fusion_is_elementor_active()) {

	require_once 'widgets/animation-controls.php';
	require_once 'widgets/position-controls.php';

	function fusion_map_meta_query($query) {
			switch ($query['value_type']) {
				case 'array':
					$value = explode(',', $query['value']);
					break;
				case 'date':
					$parts = explode(',', $query['value']);
					$value = date($parts[0], strtotime($parts[1]));
					break;
				default:
					$value = $query['value'];
					break;
			}
			$meta_query = array (
				'key' => $query['key'],
				'value' => $value,
				'compare' => $query['compare'],
				'type' => $query['type'],
			);

		return $meta_query;
	}

	/**
	 * Get items by post settings
	 */
	function fusion_get_post_items_by_settings( $settings, $post_type = 'post' ) {
		if (get_query_var('page')) {
			$paged = get_query_var('page');
		} elseif (get_query_var('paged')) {
			$paged = get_query_var('paged');
		} else {
			$paged = 1;
		}

		$meta_query = array_map("\Elementor\\fusion_map_meta_query", $settings['meta_query']);

		$post_args = array (
			'posts_per_page' => $settings['posts_per_page'], // Total
			'offset' => $settings['offset'], // Total
	    'category_name' => $settings['category'], 
	    'post__in' => !empty(trim($settings['include'])) ? explode( ',', $settings['include']) : '', 
	    'post__not_in' => explode( ',', $settings['exclude']), 
	    'orderby' => $settings['orderby'],
	    'order' => $settings['order'],
	    'meta_query' => $meta_query,
	    'post_type' => $post_type,
	    'paged' => $paged,
		);
		$posts = new \WP_Query($post_args);

		return $posts;
	}

	/**
	 * Add theme category
	 */
	function fusion_elementor_init() {
    Plugin::instance()->elements_manager->add_category(
      'fusion-widgets',
      [
          'title'  => 'Fusion_ Widgets',
          'icon' => 'font'
      ],
      1
    );
	}
	add_action('elementor/init','Elementor\fusion_elementor_init');

	/**
	 * Add widgets
	 */
	function fusion_add_widgets() {
		require_once 'widgets/custom-map.php';
		require_once 'widgets/buttons.php';
		require_once 'widgets/image-gallery.php';
		require_once 'widgets/carousel-base.php';
		require_once 'widgets/carousel-portfolio.php';
		require_once 'widgets/carousel-post.php';
		require_once 'widgets/carousel-content.php';
		require_once 'widgets/carousel-testimonial.php';
		require_once 'widgets/grid-base.php';
		require_once 'widgets/grid-image.php';
		require_once 'widgets/grid-portfolio.php';
		require_once 'widgets/grid-post.php';
		require_once 'widgets/grid-product.php';
		require_once 'widgets/grid-custom-post-types.php';
		require_once 'widgets/team-member.php';
		require_once 'widgets/parallax-controls.php';
		require_once 'widgets/query-controls.php';
	}
	add_action('elementor/widgets/widgets_registered','Elementor\fusion_add_widgets');

}

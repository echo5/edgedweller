<?php

/**
 * Bootstrap classes
 */
require 'vendor/wp-bootstrap-navwalker.php';
require 'vendor/bootstrap-comment-walker.php';

/**
 * Enqueue admin scripts and styles.
 */
function fusion_admin_scripts() {
  wp_enqueue_script( 'fusion-admin', get_template_directory_uri() . '/assets/js/admin.js', array( 'wp-color-picker' ), '1.2.2', true );
}
add_action( 'admin_enqueue_scripts', 'fusion_admin_scripts' );

/**
 * Image sizes
 */
add_image_size( 'wide', 470, 300, true ); // 1.56
add_image_size( 'medium', 560, 450, true ); // 1.24
add_image_size( 'tall', 470, 600, true ); // 0.78
add_image_size( 'taller', 470, 700, true ); // 0.67
function fusion_add_image_sizes( $sizes ) {
  return array_merge( $sizes, array(
    'wide' => __( 'Wide', 'fusion' ),
    'medium' => __( 'Medium', 'fusion' ),
    'tall' => __( 'Tall', 'fusion' ),
    'taller' => __( 'Taller', 'fusion' ),
  ) );
}
add_filter( 'image_size_names_choose', 'fusion_add_image_sizes' );

/**
 * Set default date format
 *
 * @return string
 */
function fusion_default_date_format() {
	return 'n F, Y';
}

/**
 * Set header styles
 *
 * @return array
 */
function fusion_header_styles() {
  return array(
    'default' => 'Default',
    'nav-left' => 'Navigation Left',
    'floating' => 'Floating',
    'overlay' => 'Overlay',
    'side-collapsed' => 'Side Collapsed',
    'side' => 'Side Navigation',
    'slide-down' => 'Slide Down',
  );
}
add_filter( 'luxe_header_styles', 'fusion_header_styles' );

/**
 * Set portfolio styles
 *
 * @return array
 */
function fusion_portfolio_single_styles() {
  return array(
    'carousel-right' => 'Carousel Right',
    'meta-right' => 'Meta Right',
    'images-bottom' => 'Images Bottom',
    'images-right' => 'Images Right',
    'slider-right' => 'Slider Right',
  );
}
add_filter( 'luxe_portfolio_single_styles', 'fusion_portfolio_single_styles' );

/**
 * Set portfolio styles
 *
 * @return array
 */
function fusion_portfolio_grid_styles() {
  return array(
    'default' => 'Default',
    'gradient-bottom' => 'Gradient Bottom',
    'label' => 'Label',
    'layered' => 'Layered',
    'overlay' => 'Overlay',
    'overlay-left' => 'Overlay Left',
    'overlay-excerpt' => 'Overlay with Excerpt',
    'overlay-inset' => 'Overlay Inset',
    'clip-text' => 'Clip Text',
  );
}
add_filter( 'luxe_portfolio_grid_styles', 'fusion_portfolio_grid_styles' );

/**
 * Set post styles
 *
 * @return array
 */
function fusion_post_single_styles() {
  return array(
    'default' => 'Default',
    'card' => 'Card',
    'featured-left' => 'Featured Left',
    'featured-top' => 'Featured Top',
    'sidebar' => 'Sidebar',
  );
}
add_filter( 'luxe_post_single_styles', 'fusion_post_single_styles' );

/**
 * Set post styles
 *
 * @return array
 */
function fusion_post_grid_styles() {
  return array(
    'image-left' => 'Image Left',
    'default' => 'Default',
    'card' => 'Card Excerpt',
    'excerpt' => 'With Excerpt',
    'gradient-bottom' => 'Gradient Bottom',
    'reverse-overlay' => 'Reverse Overlay',
    'no-image' => 'No Image',
  );
}
add_filter( 'luxe_post_grid_styles', 'fusion_post_grid_styles' );

/**
 * Strip first gallery to use as featured content
 *
 * @return string
 */
function fusion_strip_first_gallery( $output, $attr ) {
    if ( is_singular('portfolio') ) {
    	remove_filter( current_filter(), __FUNCTION__ );
    	return '<!-- gallery removed -->';
    }
}

add_filter( 'post_gallery', 'fusion_strip_first_gallery', 10, 2 );

/**
 * Add meta box to portfolio
 */
function fusion_add_portfolio_meta_box()
{
    $screens = ['portfolio'];
    foreach ($screens as $screen) {
        add_meta_box(
            'portfolio_details',
            'Portfolio Details',
            'fusion_portfolio_meta_box_html',
            $screen,
            'side',
            'core'
        );
    }
}
add_action('add_meta_boxes', 'fusion_add_portfolio_meta_box');

/**
 * Portfolio meta box html
 */
function fusion_portfolio_meta_box_html($post)
{
  wp_enqueue_style( 'wp-color-picker');
  wp_enqueue_script( 'wp-color-picker');
	$post_meta = get_post_meta($post->ID);
  ?>
  <label for="made_by">Made By</label>
  <input type="text" name="made_by" id="made_by" class="postbox" value="<?php echo (isset($post_meta['_made_by'])? $post_meta['_made_by'][0] : ''); ?>">
  <label for="client">Client</label>
  <input type="text" name="client" id="client" class="postbox" value="<?php echo (isset($post_meta['_client'])? $post_meta['_client'][0] : ''); ?>">
  <label for="date">Date</label>
  <input type="text" name="date" id="date" class="postbox" value="<?php echo (isset($post_meta['_date'])? $post_meta['_date'][0] : ''); ?>">
  <label for="role">Role</label>
  <input type="text" name="role" id="role" class="postbox" value="<?php echo (isset($post_meta['_role'])? $post_meta['_role'][0] : ''); ?>">
  <label for="role">Accent Color</label>
  <input type="text" name="color" id="color" class="postbox color-picker" data-alpha="true" value="<?php echo (isset($post_meta['_color'])? $post_meta['_color'][0] : ''); ?>">
  <?php
}

/**
 * Save portfolio meta info
 */
function fusion_save_portfolio_post_meta($post_id)
{
	global $post;
  if ($post && $post->post_type == 'portfolio') {
    if ($_POST['made_by']) {
      update_post_meta( $post_id, '_made_by', esc_html($_POST['made_by']) );
    }
    if ($_POST['client']) {
      update_post_meta( $post_id, '_client', esc_html($_POST['client']) );
    }
    if ($_POST['date']) {
      update_post_meta( $post_id, '_date', esc_html($_POST['date']) );
    }
    if ($_POST['role']) {
      update_post_meta( $post_id, '_role', esc_html($_POST['role']) );
    }
    if ($_POST['color']) {
      update_post_meta( $post_id, '_color', esc_html($_POST['color']) );
    }
  }
}
add_action('save_post', 'fusion_save_portfolio_post_meta');

/**
 * Register widget area.
 */
function fusion_widgets_init() {
  register_sidebar( array(
    'name'          => esc_html__( 'Sidebar', 'fusion' ),
    'id'            => 'sidebar-1',
    'description'   => esc_html__( 'Add widgets here.', 'fusion' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => esc_html__( 'Header 1', 'fusion' ),
    'id'            => 'header-1',
    'description'   => esc_html__( 'Add widgets here.', 'fusion' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => esc_html__( 'Header 2', 'fusion' ),
    'id'            => 'header-2',
    'description'   => esc_html__( 'Add widgets here.', 'fusion' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => esc_html__( 'Footer 1', 'fusion' ),
    'id'            => 'footer-1',
    'description'   => esc_html__( 'Add widgets here.', 'fusion' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => esc_html__( 'Fixed Corner Left', 'fusion' ),
    'id'            => 'fixed-corner-left',
    'description'   => esc_html__( 'Fixed corner widget on bottom left of screen at all times.', 'fusion' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => esc_html__( 'Fixed Corner Right', 'fusion' ),
    'id'            => 'fixed-corner-right',
    'description'   => esc_html__( 'Fixed corner widget on bottom right of screen at all times.', 'fusion' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

}
add_action( 'widgets_init', 'fusion_widgets_init' );

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function fusion_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'fusion_custom_excerpt_length', 999 );

/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function fusion_excerpt_more_ellipsis( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'fusion_excerpt_more_ellipsis' );

/**
 * Theme options
 */
require get_template_directory() . '/inc/options.php';

/**
 * Grid styles and classes
 */
function fusion_grid_styles() {
  return [
    'masonry' => [
      [
        'classes' =>'h-470',
        'image_size' => 'tall'
      ],
      [
        'classes' =>'h-350',
        'image_size' => 'wide'
      ],
      [
        'classes' =>'h-670',
        'image_size' => 'taller'
      ],
      [
        'classes' =>'h-470',
        'image_size' => 'tall'
      ],
      [
        'classes' =>'h-470',
        'image_size' => 'tall'
      ],
      [
        'classes' =>'h-600',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'h-350',
        'image_size' => 'wide'
      ],
      [
        'classes' =>'h-670',
        'image_size' => 'taller'
      ],
    ],
    'wide-tall' => [
      [
        'classes' =>'col-md-4 h-250',
        'image_size' => 'wide'
      ],
      [
        'classes' =>'col-md-4 h-450',
        'image_size' => 'tall'
      ],
      [
        'classes' =>'col-md-4 h-350',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 h-450',
        'image_size' => 'wide'
      ],
      [
        'classes' =>'col-md-4 h-350',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 h-250',
        'image_size' => 'wide'
      ],
    ],
    'scattered-center' => [
      [
        'classes' =>'col-md-3 offset-md-2 h-250',
        'image_size' => 'wide'
      ],
      [
        'classes' =>'col-md-4 offset-md-right-2 h-450',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 offset-md-1 h-450',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-2 h-250',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 h-450',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 offset-md-3 h-450',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-2 offset-md-right-2 h-250',
        'image_size' => 'medium'
      ],
    ],
    '2-column-alternating' => [
      [
        'classes' =>'col-md-6 h-670',
        'image_size' => 'tall'
      ],
      [
        'classes' =>'col-md-6 h-350',
        'image_size' => 'wide'
      ],
      [
        'classes' =>'col-md-6 h-670',
        'image_size' => 'tall'
      ],
      [
        'classes' =>'col-md-6 h-350',
        'image_size' => 'wide'
      ],
    ],
    '2-column-alternating-spaced' => [
      [
        'classes' =>'col-md-5 h-670',
        'image_size' => 'wide'
      ],
      [
        'classes' =>'col-md-5 offset-md-2 h-350 mt-160 mb-160',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-5 h-350 mt-160 mb-160',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-5 offset-md-2 h-670',
        'image_size' => 'medium'
      ],
    ],
    'single-focal-point' => [
      [
        'classes' =>'col-md-8 h-600',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => 'medium'
      ],
      // Second set
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-8 h-600',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 h-300',
        'image_size' => 'medium'
      ],
    ],
    'scattered' => [
      [
        'classes' =>'col-md-4 mb-5 h-250',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-7 mb-5 offset-md-1 h-670',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 mb-5 h-600',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-7 mb-5 offset-md-1 h-470',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 mb-5 h-450',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-3 offset-md-1 h-250',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 h-600',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-7 mb-3 h-670',
        'image_size' => 'medium'
      ],
      [
        'classes' =>'col-md-4 offset-md-1 mb-5 h-300',
        'image_size' => 'medium'
      ],
    ],
  ];
}
add_filter( 'luxe_grid_styles', 'fusion_grid_styles' );

/**
 * Grid style options for dropdown
 */
function fusion_grid_style_options($default_options) {
  $theme_grid_options = array();
  $grid_styles = fusion_grid_styles();
  foreach ($grid_styles as $style => $settings) {
    $theme_grid_options[$style] = ucfirst(str_replace('-', ' ', $style));
  }
  return array_merge($default_options, $theme_grid_options);
}
add_filter( 'luxe_grid_style_options', 'fusion_grid_style_options' );


/**
 * Barba ajax
 */
function fusion_ajax_scripts() {
  wp_enqueue_style( 'elementor-frontend' );
  wp_enqueue_script( 'wc-single-product' );
  wp_enqueue_style( 'font-awesome' );
  wp_enqueue_script( 'jquery-slick' );
}
add_action( 'wp_enqueue_scripts', 'fusion_ajax_scripts', 10 );

function fusion_ajax_vars() {
  $script_variables = array(
    'ajax' => get_theme_mod( 'ajax', false ),
    'ajaxTransition' => get_theme_mod( 'ajax_transition', 'FadeTransition' ),
    'googleMapsApiKey' => get_theme_mod( 'google_maps_api_key', '' ),
  );
  wp_localize_script( 'fusion-main', 'themeConfig', $script_variables );
}
add_action( 'wp_enqueue_scripts', 'fusion_ajax_vars', 100 );

/**
 * Add Woocommerce support
 */
function fusion_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'fusion_woocommerce_support' );

/**
 * Woocommerce container wrapper
 */
function fusion_add_woocommerce_cart_container_before() {
    echo '<div class="container">';
}
add_action( 'woocommerce_before_cart', 'fusion_add_woocommerce_cart_container_before' );
add_action( 'woocommerce_before_checkout_form', 'fusion_add_woocommerce_cart_container_before' );

function fusion_add_woocommerce_cart_container_after() {
    echo '</div>';
}
add_action( 'woocommerce_after_cart', 'fusion_add_woocommerce_cart_container_after' );
add_action( 'woocommerce_after_checkout_form', 'fusion_add_woocommerce_cart_container_after' );

/**
 * Ajax comments
 */
function fusion_ajax_comments( $comment_ID, $comment_status ){
    if( ! empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest' ) {
        switch( $comment_status ) {
            case '0':
                wp_notify_moderator( $comment_ID );
            case '1':
                echo "success";
                $commentdata = get_comment( $comment_ID, ARRAY_A );
                $post = get_post( $commentdata['comment_post_ID'] );
                wp_notify_postauthor( $comment_ID );
                break;
            default:
                echo "error";
        }
        exit;
    }
}
add_action( 'comment_post', 'fusion_ajax_comments', 10, 2 );

/**
 * Animations array
 * @return array
 */
function fusion_animations() {
    return array (
        'callout.bounce' => 'callout.bounce',
        'callout.shake' => 'callout.shake',
        'callout.flash' => 'callout.flash',
        'callout.pulse' => 'callout.pulse',
        'callout.swing' => 'callout.swing',
        'callout.tada' => 'callout.tada',
        'transition.moveRight' => 'transition.moveRight',
        'transition.fadeIn' => 'transition.fadeIn',
        'transition.fadeOut' => 'transition.fadeOut',
        'transition.flipXIn' => 'transition.flipXIn',
        'transition.flipXOut' => 'transition.flipXOut',
        'transition.flipYIn' => 'transition.flipYIn',
        'transition.flipYOut' => 'transition.flipYOut',
        'transition.flipBounceXIn' => 'transition.flipBounceXIn',
        'transition.flipBounceXOut' => 'transition.flipBounceXOut',
        'transition.flipBounceYIn' => 'transition.flipBounceYIn',
        'transition.flipBounceYOut' => 'transition.flipBounceYOut',
        'transition.swoopIn' => 'transition.swoopIn',
        'transition.swoopOut' => 'transition.swoopOut',
        'transition.whirlIn' => 'transition.whirlIn',
        'transition.whirlOut' => 'transition.whirlOut',
        'transition.shrinkIn' => 'transition.shrinkIn',
        'transition.shrinkOut' => 'transition.shrinkOut',
        'transition.expandIn' => 'transition.expandIn',
        'transition.expandOut' => 'transition.expandOut',
        'transition.bounceIn' => 'transition.bounceIn',
        'transition.bounceOut' => 'transition.bounceOut',
        'transition.bounceUpIn' => 'transition.bounceUpIn',
        'transition.bounceUpOut' => 'transition.bounceUpOut',
        'transition.bounceDownIn' => 'transition.bounceDownIn',
        'transition.bounceDownOut' => 'transition.bounceDownOut',
        'transition.bounceLeftIn' => 'transition.bounceLeftIn',
        'transition.bounceLeftOut' => 'transition.bounceLeftOut',
        'transition.bounceRightIn' => 'transition.bounceRightIn',
        'transition.bounceRightOut' => 'transition.bounceRightOut',
        'transition.slideUpIn' => 'transition.slideUpIn',
        'transition.slideUpOut' => 'transition.slideUpOut',
        'transition.slideDownIn' => 'transition.slideDownIn',
        'transition.slideDownOut' => 'transition.slideDownOut',
        'transition.slideLeftIn' => 'transition.slideLeftIn',
        'transition.slideLeftOut' => 'transition.slideLeftOut',
        'transition.slideRightIn' => 'transition.slideRightIn',
        'transition.slideRightOut' => 'transition.slideRightOut',
        'transition.slideUpBigIn' => 'transition.slideUpBigIn',
        'transition.slideUpBigOut' => 'transition.slideUpBigOut',
        'transition.slideDownBigIn' => 'transition.slideDownBigIn',
        'transition.slideDownBigOut' => 'transition.slideDownBigOut',
        'transition.slideLeftBigIn' => 'transition.slideLeftBigIn',
        'transition.slideLeftBigOut' => 'transition.slideLeftBigOut',
        'transition.slideRightBigIn' => 'transition.slideRightBigIn',
        'transition.slideRightBigOut' => 'transition.slideRightBigOut',
        'transition.perspectiveUpIn' => 'transition.perspectiveUpIn',
        'transition.perspectiveUpOut' => 'transition.perspectiveUpOut',
        'transition.perspectiveDownIn' => 'transition.perspectiveDownIn',
        'transition.perspectiveDownOut' => 'transition.perspectiveDownOut',
        'transition.perspectiveLeftIn' => 'transition.perspectiveLeftIn',
        'transition.perspectiveLeftOut' => 'transition.perspectiveLeftOut',
        'transition.perspectiveRightIn' => 'transition.perspectiveRightIn',
        'transition.perspectiveRightOut' => 'transition.perspectiveRightOut',
    );
}

/**
 * Animation style options for dropdown
 */
function fusion_animation_options($default_options) {
  return array_merge($default_options, fusion_animations());
}
add_filter( 'luxe_animation_options', 'fusion_animation_options' );

/**
 * Add icons to elementor
 */
function fusion_add_icons_to_elementor($default_options) {
  $theme_icons = array(
    'icon-mouse' => 'icon-mouse',
    'icon-chevron-up' => 'icon-chevron-up',
    'icon-chevron-down' => 'icon-chevron-down',
    'icon-chevron-left' => 'icon-chevron-left',
    'icon-chevron-right' => 'icon-chevron-right',
    'icon-cross2' => 'icon-cross2',
    'icon-palette' => 'icon-palette',
    'icon-brush2' => 'icon-brush2',
    'icon-desktop' => 'icon-desktop',
    'icon-paper-plane' => 'icon-paper-plane',
    'icon-pencil-ruler' => 'icon-pencil-ruler',
    'icon-arrow-up' => 'icon-arrow-up',
    'icon-arrow-down' => 'icon-arrow-down',
    'icon-arrow-left' => 'icon-arrow-left',
    'icon-arrow-right' => 'icon-arrow-right',
    'icon-menu' => 'icon-menu',
    'icon-telephone' => 'icon-telephone',
    'icon-envelope' => 'icon-envelope',
    'icon-clock3' => 'icon-clock3',
  );
  return array_merge($theme_icons, $default_options);
}
add_filter( 'elementor/icons', 'fusion_add_icons_to_elementor' );


/**
 * Number of columns per woocommerce row
 */
if (!function_exists('fusion_woocommerce_loop_columns')) {
  function fusion_woocommerce_loop_columns() {
    return 3;
  }
}
add_filter('loop_shop_columns', 'fusion_woocommerce_loop_columns');

/**
 * Add data attibutes to body tag
 */
function fusion_add_body_atts($atts) {
  // Overlay header scheme
  $offcanvas_header_scheme = get_theme_mod( 'offcanvas_header_scheme', 'light' );
  $atts['data-offcanvas-header-scheme'] = $offcanvas_header_scheme;
  return $atts;
}
add_filter('luxe_body_atts', 'fusion_add_body_atts', 10, 1);
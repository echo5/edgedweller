<?php
/**
 * Helper functions to reduce redundancy
 *
 * @package fusion
 */

/**
 * Load a template into a string for later output
 *
 * @param string $template_name
 * @param string $part_name
 * @return string
 */
function load_template_part( $template_name, $params = array(), $output = false ) {
    if(!$output) ob_start();
    $template_file = locate_template('template-parts/'. $template_name .'.php', false, false);
    extract($params, EXTR_SKIP);
    if ($template_file) include( $template_file );
    if(!$output) return ob_get_clean();
}

/**
 * Get size information for all currently-registered image sizes.
 *
 * @global $_wp_additional_image_sizes
 * @uses   get_intermediate_image_sizes()
 * @return array $sizes Data for all currently-registered image sizes.
 */
function get_image_sizes() {
	global $_wp_additional_image_sizes;

	$sizes = array();

	foreach ( get_intermediate_image_sizes() as $_size ) {
		if ( in_array( $_size, array('thumbnail', 'medium', 'medium_large', 'large') ) ) {
			$sizes[ $_size ] = $_size . ' (' . get_option( "{$_size}_size_w" ) . 'x' . get_option( "{$_size}_size_h" ) . ')';
			// $sizes[ $_size ]['crop']   = (bool) get_option( "{$_size}_crop" );
		} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
			$sizes[ $_size ] = $_size . ' (' . $_wp_additional_image_sizes[ $_size ]['width'] . 'x' . $_wp_additional_image_sizes[ $_size ]['height'] . ')';
		}
	}

	return $sizes;
}

/**
 * Add attributes to body tag
 * 
 * @param array $atts
 * @return string $output
 */
function body_atts($atts = array()) {
	$output = '';
	$atts = apply_filters('luxe_body_atts', $atts);
	foreach($atts as $key => $att) {
		$output .= $key . '="' . $att .'" ';
	}
	echo $output;
}

/**
 * Get set portfolio style or theme option
 *
 * @return string
 */
function fusion_get_portfolio_grid_style() {
	$styles = fusion_portfolio_grid_styles();
	if ( count($styles) > 0)
		$first_style = key($styles);

	if (isset($GLOBALS['portfolio_grid_style']))
		return $GLOBALS['portfolio_grid_style'];

	return get_theme_mod('portfolio_grid_style', $first_style);
}

/**
 * Get set post style or theme option
 *
 * @return string
 */
function fusion_get_post_single_style() {
	$styles = fusion_post_single_styles();
	if ( count($styles) <= 1)
		return 'default';

	if (isset($GLOBALS['post_single_style']))
		return $GLOBALS['post_single_style'];

	return get_theme_mod('post_single_style', 'default');
}

/**
 * Get set portfolio style or theme option
 *
 * @return string
 */
function fusion_get_post_grid_style() {
	$styles = fusion_post_grid_styles();
	if ( count($styles) > 0)
		$first_style = key($styles);

	if (isset($GLOBALS['post_grid_style']))
		return $GLOBALS['post_grid_style'];

	return get_theme_mod('post_grid_style', $first_style);
}

/**
 * Get set portfolio style or theme option
 *
 * @return string
 */
function fusion_get_portfolio_single_style() {
	$styles = fusion_portfolio_single_styles();
	if ( count($styles) > 0)
		$first_style = key($styles);

	if (!empty($GLOBALS['portfolio_style']))
		return $GLOBALS['portfolio_style'];

	return get_theme_mod('portfolio_single_style', $first_style);
}


/**
 * Get portfolio categories
 *
 * @return array
 */
function fusion_get_portfolio_categories() {
	global $post;
	$categories = [];
	$terms = get_the_terms( $post->ID , 'portfolio_category' );
	if ($terms) {
		foreach ( $terms as $term ) {
			$categories[] = $term->name;
		}
	}
	return $categories;
}

/**
 * Get header style
 *
 * @return string
 */
function fusion_get_header_style() {
	$styles = fusion_header_styles();
	if ( count($styles) > 0)
		$first_style = key($styles);

	return get_theme_mod('header_style', $first_style);
}

/**
 * Check if Woocommerce is active
 *
 * @return boolean
 */
function fusion_is_woocommerce_active() {
    return class_exists( 'WooCommerce' );
}

/**
 * Check if Elementor is active
 *
 * @return boolean
 */
function fusion_is_elementor_active() {
	return defined('ELEMENTOR_VERSION');
}

/**
 * Check if Kirki is active
 *
 * @return boolean
 */
function fusion_is_kirki_active() {
  return class_exists( 'Kirki' );
}

/**
 * Strip specific shortcode
 *
 * @param string $code name of the shortcode
 * @param string $content
 * @return string content with shortcode striped
 */
function fusion_strip_shortcode($code, $content)
{
    global $shortcode_tags;

    $stack = $shortcode_tags;
    $shortcode_tags = array($code => 1);

    $content = strip_shortcodes($content);

    $shortcode_tags = $stack;
    return $content;
}
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fusion
 */

get_header(); ?>

	<?php
	while ( have_posts() ) : the_post();

		get_template_part( 'template-parts/content', 'page' );

			if ( comments_open() || get_comments_number() ) : ?>
				<div class="col-md-8 offset-md-2 mt-5">
					<?php comments_template(); ?>
				</div>
			<?php 
			endif;

	endwhile;
	?>

<?php
get_footer();

<div class="page-header inverse text-center" style="background-image: url('<?php the_post_thumbnail_url('full'); ?>');">
	<div class="container">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php get_template_part('template-parts/post/meta'); ?>
	</div>

</div>

<div class="container">
	<div class="card card-main">
		<div class="card-block">
			<div class="row">
				<div class="col-md-8 offset-md-2">
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
					<footer>
						<div class="row">
							<div class="col-md-6">
								<div class="tags">
									<?php the_tags('', '', ''); ?>
								</div>
							</div>
							<div class="col-md-6">
								<?php echo fusion_social_share(get_the_ID(), get_the_title()); ?>
							</div>
						</div>
					</footer>
				</div>
			</div>
		</div>
	</div>
	<div class="post-nav-container">
		<?php fusion_post_navigation(); ?>
	</div>
	<div class="card">
		<div class="card-block">
			<div class="col-md-8 offset-md-2">
				<?php
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
				?>
			</div>
		</div>
	</div>
</div>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="featured mb-3">
		<?php the_post_thumbnail($GLOBALS['image_size']); ?>
		<div class="overlay">
			<div class="overlay-inner">
				<div class="categories mb-3">
					<?php the_category(', '); ?>
				</div>
				<div class="excerpt">
					<?php the_excerpt(); ?>
				</div>
				<a href="<?php echo get_the_permalink(); ?>" class="read-more h5 mt-2">Read More</a>
			</div>
			<a href="<?php the_permalink(); ?>" class="overlay-link"></a>
		</div>
	</div><!-- .featured -->
	
	<a href="<?php echo the_permalink(); ?>">
		<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>
	</a>
	<div class="post-meta mt-3">
		<span class="author"><?php the_author(); ?></span>
		<span class="date"><?php the_date(fusion_default_date_format()); ?></span>
	</div>

</article><!-- #post-## -->
<div class="container-fluid">
	<div class="row">
		<div class="featured col-md-4" style="background-image: url('<?php the_post_thumbnail_url('full'); ?>');">
		</div>
		<div class="col-md-8 offset-md-4 main stick-on-scroll">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="page-header">
					<div class="row">
						<div class="col-md-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div>
						<div class="col-md-12 post-meta-col mt-3 mb-3">
							<?php get_template_part('template-parts/post/meta'); ?>
						</div>
					</div>
					<span class="separator"></span>
				</div>

				<div class="entry-content">
					<?php the_content(); ?>
				</div><!-- .entry-content -->

				<footer class="entry-footer">
					<div class="h3 post-nav-container">
						<?php fusion_post_navigation(); ?>
					</div>
				</footer><!-- .entry-footer -->
			</article><!-- #post-## -->

			<?php
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			?>
		</div>
	</div>

</div>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<div class="container">
			<header class="entry-header page-header">
				<div class="row">
					<div class="col-md-6">
						<?php the_title( '<h1 class="entry-title mb-5">', '</h1>' ); ?>
					</div>
					<div class="col-md-6">
						<?php get_template_part('template-parts/portfolio/details'); ?>
					</div>
				</div>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php
					the_content();
				?>
			</div><!-- .entry-content -->

			<div class="featured">
				<?php
				if ( get_post_gallery() ) :
					$gallery = get_post_gallery( get_the_ID(), false );
					$slick_options = [
						'slidesToShow' => 1,
						'arrows' => true,
					];
					?>
					<div class="luxe-grid row">
					<?php

					// include get_template_directory() . '/inc/widgets/grid-base.php';
					// $grid = new Elementor\Widget_Grid_Base();

					// echo $grid;
					// foreach( $gallery['src'] as $src ) : 
					// 		<div class="grid-item col-md-4">
					// 			<div class="featured">
					// 		    <img src="<?php echo $src;
					?>
					</div>
					<?php
				endif; 
				?>
			</div><!-- .featured -->

			<footer class="fixed">
				<?php the_post_navigation(); ?>
			</footer>

		</div>
	
</article><!-- #post-## -->
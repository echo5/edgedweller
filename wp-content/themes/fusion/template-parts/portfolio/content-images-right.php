<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 offset-md-1 main stick-on-scroll">
				<header class="entry-header page-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					<div class="categories">
						<?php echo implode(', ', fusion_get_portfolio_categories()); ?>
					</div>
					<span class="separator"></span>
				</header><!-- .entry-header -->


				<div class="entry-content">
					<?php
						the_content();
					?>
				</div><!-- .entry-content -->

				<?php get_template_part('template-parts/portfolio/details'); ?>
			
				<footer>
					<?php fusion_post_navigation_minimal(); ?>
				</footer>

			</div>

			<div class="col-md-6 offset-md-1 sidebar col-featured">
				<div class="featured">
					<?php
					if ( get_post_gallery() ) :
					    echo get_post_gallery();
				  else:
				  	the_post_thumbnail('full');
					endif; 
					?>
				</div><!-- .featured -->
			</div>
		</div>
	</div>
</article><!-- #post-## -->
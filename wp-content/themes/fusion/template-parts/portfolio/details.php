<div class="portfolio-details">
	<?php 
	$details = [
		'_made_by' => 'Made By',
		'_date' => 'Date',
		'_client' => 'Client', 
		'_role' => 'Role'
	];
	foreach ($details as $key => $detail):
		if ($value = get_post_meta(get_the_ID(), $key)):
			?> 
			<span class="detail detail-<?php echo $key; ?>">
				<span class="detail-name h5"><?php echo $detail; ?></span>
				<span class="detail-value"><?php echo $value[0]; ?></span>
			</span>
			<?php
		endif;
	endforeach;
	?>
</div>
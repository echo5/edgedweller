<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="featured">
		<?php the_post_thumbnail($GLOBALS['image_size']); ?>
	</div><!-- .featured -->
	
	<div class="overlay overlay-gradient">
		<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>
		<div class="categories">
			<?php echo implode(', ', fusion_get_portfolio_categories()); ?>
		</div>
		<a href="<?php the_permalink(); ?>" class="overlay-link"></a>
	</div>

</article><!-- #post-## -->
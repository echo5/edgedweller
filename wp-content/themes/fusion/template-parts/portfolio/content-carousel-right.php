<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-5 main stick-on-scroll">
				<header class="entry-header page-header">
					<span class="separator"></span>
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					<div class="categories">
						<?php echo implode(', ', fusion_get_portfolio_categories()); ?>
					</div>
				</header><!-- .entry-header -->


				<div class="entry-content">
					<?php
						the_content();
					?>
				</div><!-- .entry-content -->

				<?php get_template_part('template-parts/portfolio/details'); ?>
				
				<footer>
					<?php fusion_post_navigation_minimal(); ?>
				</footer>
			</div>

			<div class="col-md-7 col-featured">
				<div class="elementor-slick-slider stick-on-scroll">
					<div class="sidebar featured featured-carousel" >
						<?php
						if ( get_post_gallery() ) :
							$gallery = get_post_gallery( get_the_ID(), false );
							$slick_options = [
								'slidesToShow' => 2,
								'infinite' => true,
								'arrows' => true,
								'variableWidth' => false,
							];
							?>
							<div class="carousel-wrapper" data-carousel-settings="<?php echo esc_attr( wp_json_encode( $slick_options ) ); ?>">
								<div class="carousel slick-arrows-inside">
								<?php
								foreach( $gallery['src'] as $src ) : ?>
										<div class="slide">
									    <img src="<?php echo $src; ?>" />
								    </div>
								    <?php
								endforeach;
								?>
								</div>
							</div>
							<?php
					  else:
					  	the_post_thumbnail('full');
						endif; 
						?>
					</div><!-- .featured -->
				</div>
			</div>
		</div>
	</div>
</article><!-- #post-## -->
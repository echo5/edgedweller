<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6 main stick-on-scroll">
				<header class="entry-header page-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					<div class="categories">
						<?php echo implode(', ', fusion_get_portfolio_categories()); ?>
					</div>
					<span class="separator"></span>
				</header><!-- .entry-header -->


				<div class="entry-content">
					<?php
						the_content();
					?>
				</div><!-- .entry-content -->

				<?php get_template_part('template-parts/portfolio/details'); ?>
			
				<footer>
					<?php fusion_post_navigation_minimal(); ?>
				</footer>

			</div>

			<div class="col-md-6 col-featured">
				<div class="featured featured-carousel stick-on-scroll">
					<?php
					if ( get_post_gallery() ) :
						$gallery = get_post_gallery( get_the_ID(), false );
						$slick_options = [
							'slidesToShow' => 1,
							'arrows' => true,
						];
						?>
						<div class="carousel stretched" data-slider_options="<?php echo esc_attr( wp_json_encode( $slick_options ) ); ?>" style="position: relative;">
						<?php
						foreach( $gallery['src'] as $src ) : ?>
								<div class="slide">
							    <img src="<?php echo $src; ?>" />
						    </div>
						    <?php
						endforeach;
						?>
						</div>
						<?php
				  else:
				  	the_post_thumbnail('full');
					endif; 
					?>
				</div><!-- .featured -->
			</div>
		</div>
	</div>
</article><!-- #post-## -->
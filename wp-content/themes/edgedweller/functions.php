<?php

/**
 * Enqueue child theme styles
 */
function ew_enqueue_styles() {
    wp_enqueue_style('fusion-main', get_template_directory_uri() .'/style.css');
    wp_enqueue_style('fusion-demo',
        get_stylesheet_directory_uri() . '/style.css'
    );
		wp_enqueue_script( 'ew-main', get_stylesheet_directory_uri() . '/assets/js/main.js', array('jquery'), '', true );
}
add_action('wp_enqueue_scripts', 'ew_enqueue_styles', 100);

/**
 * Remove autop from widgets
 */
remove_filter('widget_text_content', 'wpautop');

/**
 * Edgedweller functions
 */
require get_stylesheet_directory() . '/inc/content-locking.php';
require get_stylesheet_directory() . '/inc/companies.php';
require get_stylesheet_directory() . '/inc/my-account.php';
require get_stylesheet_directory() . '/inc/invoicing.php';
require get_stylesheet_directory() . '/inc/assessments.php';


function ew_metaboxes( $meta_boxes ) {
	$prefix = 'ew-';

	$meta_boxes[] = array(
		'id' => 'product_lock',
		'title' => esc_html__( 'Product Lock', 'ew' ),
		'post_types' => array( 'post', 'wpt_test' ),
		'context' => 'side',
		'priority' => 'high',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => $prefix . 'product_lock',
				'type' => 'post',
				'name' => esc_html__( 'Product', 'ew' ),
				'desc' => esc_html__( 'Product that must be purchased to view this item.', 'ew' ),
				'post_type' => 'product',
				'field_type' => 'select_advanced',
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'ew_metaboxes' );
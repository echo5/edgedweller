<?php
/**
 * The template for displaying assessment/test posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package fusion
 */

get_header();

	while ( have_posts() ) : the_post();

		?>

		<?php the_content(); ?>

		<?php

	endwhile; // End of the loop.

get_footer();

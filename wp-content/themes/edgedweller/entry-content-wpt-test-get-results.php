<?php

/* @var $content string */
/* @var $renderer WpTesting_Doer_IRenderer */
/* @var $test WpTesting_Model_Test */
/* @var $passing WpTesting_Model_Passing[] */
/* @var $scales WpTesting_Model_Scale[] */
/* @var $results WpTesting_Model_Result[] */
/* @var $isShowScales boolean */
/* @var $isShowDescription boolean */
?>
<div class="page-header">
	<div class="container">
		<h1><?php echo $test->getTitle() ?></h1>
		<?php $user = get_user_by('id', $passing->getRespondentId()); ?>
	</div>
</div>
<div class="wpt_test get_results container">

<div class="results section-padded">

    <?php foreach ($results as $i => $result): /* @var $result WpTesting_Model_Result */ ?>
			<h2 class="company mb-5"><?php echo ew_get_user_company_name($user); ?></h2>

			<h3 class="user"><?php echo $user->display_name ?></h3>

			<h4 class="<?php echo $result->getCssClass($i) ?> title"><?php echo $result->getTitle() ?></h4>

			<h5 class="mb-3"><?php echo date('M d, Y', strtotime($passing->getCreated())); ?></h5>

        <div class="<?php echo $result->getCssClass($i) ?> description"><?php echo $renderer->renderWithMoreSplitted($renderer->renderTextAsHtml($result->getDescription())) ?></div>

    <?php endforeach ?>

<?php if ($isShowScales && count($results)): ?>
    <hr/>
<?php endif ?>

<?php if ($isShowScalesDiagram): ?>
    <div class="scales diagram"></div>
<?php endif ?>

<?php if ($isShowScales): ?>

    <?php foreach ($scales as $i => $scale): /* @var $scale WpTesting_Model_Scale */ ?>

        <h3 class="<?php echo $scale->getCssClass($i) ?> title"><?php echo $scale->getTitle() ?></h4>

        <div class="<?php echo $scale->getCssClass($i) ?> scores">
            <?php echo $scale->formatValueAsOutOf() ?>
        </div>
        <div class="<?php echo $scale->getCssClass($i) ?> meter">
            <span style="width: <?php echo $scale->getValueAsRatio()*100 ?>%"></span>
        </div>

        <div class="<?php echo $scale->getCssClass($i) ?> description"><?php echo $renderer->renderWithMoreSplitted($renderer->renderTextAsHtml($scale->getDescription())) ?></div>

    <?php endforeach ?>

<?php endif ?>

</div>

<?php if ($isShowDescription): ?>

<hr/>

<div class="content">
    <?php echo $content ?>
</div>

<?php endif ?>

</div>

<?php

/**
 * Add employees route
 */
function ew_add_employees_tab_endpoint() {
    add_rewrite_endpoint( 'employees', EP_ROOT | EP_PAGES );
}
 
add_action( 'init', 'ew_add_employees_tab_endpoint' );
 
/**
 * Add remove user query var
 */
function ew_add_query_vars( $vars ){
  $vars[] = "remove_user";
  return $vars;
}
add_filter( 'query_vars', 'ew_add_query_vars' );

/**
 * Add query var
 */
function ew_employees_tab_query_vars( $vars ) {
    $vars[] = 'employees';
    return $vars;
}
 
add_filter( 'query_vars', 'ew_employees_tab_query_vars', 0 );
 
/**
 * Add employees tab
 */
function ew_add_employees_tab_link_my_account( $items ) {
	$user = wp_get_current_user();
	$newItems = $items;
	if (in_array('company', $user->roles, true)) {
		$offset = 1;
		$newItems = array_slice($items, 0, $offset, true) +
		            array('employees' => 'Employees') +
		            array_slice($items, $offset, NULL, true);
  }
  return $newItems;
}
 
add_filter( 'woocommerce_account_menu_items', 'ew_add_employees_tab_link_my_account' );
 
/**
 * Add emplyees tab content
 */
function ew_employees_tab_content() {

	echo '<h3>Employees</h3><p>Add any company employees you\'d like to have access to assessments and workshops below. <i>Please note that these employees will have unlimited access to assessments and workshops and charges will be applied to your invoice.</i></p>';
	echo do_shortcode( '[contact-form-7 id="691" title="Employee Registration"]' );

	// Remove users
	if ($remove_user = get_query_var( 'remove_user', false )) {
		if ( ew_remove_user_from_company(get_user_by('id', $remove_user), wp_get_current_user()) ) {
			echo '<div class="alert alert-success">User removed from your account.</div>';
		} else {
			// echo '<div class="alert alert-warning">User not found or has already been removed.</div>';
		}
	}

	// Users table
	$employees = ew_get_employees(wp_get_current_user());
	if (count($employees)) {
		echo '<table class="table">';
		echo '<tr><th>Name</th><th>Email</th><th></th>';
		foreach ($employees as $employee) {
			echo '<tr>';
			echo '<td>' . $employee->display_name . '</td>';
			echo '<td>' . $employee->user_email . '</td>';
			echo '<td><a href="?remove_user=' . $employee->ID . '">Remove</a></td>';
			echo '</tr>';
		}
		echo '</table>';
		// echo '<small class="text-muted">Removing employees will not delete their account, but unlink them from the company and prevent further transactions on the company account.</small>';
	} 
	else {
		echo 'No employees have been added yet';
	}
}
add_action( 'woocommerce_account_employees_endpoint', 'ew_employees_tab_content' );
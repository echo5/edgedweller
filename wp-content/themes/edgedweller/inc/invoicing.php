<?php

/**
 * Add product to company invoice
 */
function ew_add_product_to_company_invoice($order, $product_id) {
	$product = wc_get_product($product_id);
  $user = wp_get_current_user();
  $product->set_name($product->get_name() . " (User #{$user->ID} {$user->display_name})");
	$order->add_product($product, 1);
	$order->update_status('on-hold');
	$order->calculate_shipping();
	$order->calculate_totals();
}

/**
 * Send invoices
 */
function ew_send_invoices() {
  $orders = wc_get_orders( array(
      'limit'    => -1,
      'status'   => array( 'wc-on-hold' )
  ) );

  // Set on hold orders to pending to push invoices
  foreach ( $orders as $order ) {
    $order->update_status('pending');
  }
}

/**
 * Callback if invoice times not set
 */
if (!wp_next_scheduled( 'ew_monthly_invoice' )) {
	wp_schedule_single_event( time() + 60, 'ew_schedule_invoicing' );
}
add_action( 'ew_schedule_invoicing', 'ew_schedule_invoice_times' );

/**
 * Schedule monthly invoice times
 */
function ew_schedule_invoice_times() {
  $date = date('d');
  if (!wp_next_scheduled('ew_monthly_invoice')) {
    $date = (intval($date) < 15) ? '15' : '01';
  }
  if ('01' == $date || '15' == $date) {
    switch ($date) {
      case '01' :
        wp_schedule_single_event( strtotime('first day of next month'), 'ew_monthly_invoice' );
      break;
      case '15' :
        wp_schedule_single_event( strtotime('+14 days',strtotime('first day of')), 'ew_monthly_invoice' );
      break;
    }
  }
}
add_action('ew_monthly_invoice', 'ew_send_invoices');
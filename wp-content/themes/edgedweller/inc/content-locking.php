<?php

/**
 * Registration link
 */
function ew_login_link() {
	echo '<a href="' . get_permalink(wc_get_page_id('myaccount')) . '">' . __('Register', 'theme_name') . '</a>';
}

/**
 * Start assessment link
 */
function ew_start_link() {

}

/**
 * Hide buy buttons based on user type
 */
function ew_hide_add_to_cart_for_unregistered_users() { 
	if ( !is_user_logged_in() ) {       
	 remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
	 remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
	 remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
	 remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );  
	 add_action( 'woocommerce_single_product_summary', 'ew_login_link', 31 );
	 add_action( 'woocommerce_after_shop_loop_item', 'ew_login_link', 11 );
	}
	else {
		$current_user = wp_get_current_user();
		if ( in_array('employee', $current_user->roles, true) ) {
			add_action( 'woocommerce_single_product_summary', 'ew_start_link', 31 );
			add_action( 'woocommerce_after_shop_loop_item', 'ew_start_link', 11 );
		}
  }
}
add_action('init', 'ew_hide_add_to_cart_for_unregistered_users');
 


/**
 * Empty cart if new item added
 */
function ew_empty_cart_on_add( $cart_item_data ) {
    WC()->cart->empty_cart();
    return $cart_item_data;
}
// add_filter( 'woocommerce_add_cart_item_data', 'ew_empty_cart_on_add' );

/**
 * Add product to cart or employer invoice
 */
function ew_add_to_cart_or_invoice( $cart_item_data, $product_id ) {
    global $woocommerce;
    $current_user = wp_get_current_user();

		if ( in_array('employee', $current_user->roles, true) || in_array('company', $current_user->roles, true) ) {
	    $company = ew_get_company($current_user);
	    $order = find_or_create_company_order($company);
			ew_add_product_to_company_invoice($order, $product_id);
	    $product = wc_get_product($product_id);
			if ($post = ew_get_associated_post_from_product($product->get_id()) ) {
	    	ew_grant_customer_access( $current_user, $post->ID );
			}
			if ($post = ew_get_associated_post_from_product($product_id) ) {
				wp_redirect(get_permalink( $post->ID ));
				exit;
			}
		} else {
      WC()->cart->add_to_cart( $product_id );
		}

}
add_action('woocommerce_add_to_cart', 'ew_add_to_cart_or_invoice', 10, 2);

/**
 * Find or create new company invoice
 */
function find_or_create_company_order(\WP_User $company) {
	$args = array(
	    'post_type'   => 'shop_order',
	    'post_status' => 'wc-on-hold', 
	    'numberposts' => 1,
	    'meta_key'    => '_customer_user',
	    'meta_value'  => $company->ID,
	    'date_query' => array(
	        array(
	            'after'     => 'midnight 15 days ago',
	            'inclusive' => true,
	        ),
	    ),
	);

	$company_order = get_posts( $args );

	if (count($company_order)) {
		$order = new WC_Order( $company_order[0]->ID );
	} else {
		$order = wc_create_order();
		$order->set_customer_id($company->ID);
		$order->update_status('on-hold');
	}

	return $order;

}

/**
 * Remove quantity field
 */
function ew_remove_all_quantity_fields( $return, $product ) { return true; }
add_filter( 'woocommerce_is_sold_individually', 'ew_remove_all_quantity_fields', 10, 2 );

/**
 * Go to checkout on add to cart
 */
function ew_redirect_cart_to_checkout() {
	global $woocommerce;
	// Remove the default `Added to cart` message
	wc_clear_notices();
	return $woocommerce->cart->get_checkout_url();
}
add_filter ('woocommerce_add_to_cart_redirect', 'ew_redirect_cart_to_checkout');


/**
 * Woocommerce change 'add to cart' text
 */
function ew_custom_cart_button_text() {
    return __( 'Purchase Now', 'ew' );
}
add_filter( 'woocommerce_product_single_add_to_cart_text', 'ew_custom_cart_button_text' );

/**
 * Redirect to product page if not yet purchased
 */
function ew_product_purchase_check() {

	$current_user = wp_get_current_user();

	// skip if admin
	if( current_user_can('editor') || current_user_can('administrator') ) {
		return;
	}

	// Redirect to post if already purchased
	if (is_singular('product')) {
		if ($post = ew_get_associated_post_from_product(get_the_ID()) ) {
			if (ew_customer_has_access($current_user, $post->ID)) {
				wp_redirect(get_permalink( $post->ID ));
				exit;
			}
		}
	}

	// Redirect post to product if not purchased
	if ( is_single() ) {
		if ( $product = ew_get_associated_product_from_post(get_the_ID())) {
			if (!ew_customer_has_access($current_user, get_the_ID())) {
				wp_redirect( get_permalink( $product->get_id() ) );
				exit;
			}
		}
	}


}
add_action('template_redirect', 'ew_product_purchase_check');

/**
 * Get the content locked post from product ID
 */
function ew_get_associated_post_from_product($product_id) {
	$args = array(
	    'post_type'   => array( 'post', 'wpt_test' ),
	    'numberposts' => 1,
	    'meta_key'    => 'ew-product_lock',
	    'meta_value'  => $product_id,
	);
	$posts = get_posts( $args );

	if (count($posts)) {
		return $posts[0];
	} 
	return false;
}

/**
 * Get the product from a content locked post ID
 */
function ew_get_associated_product_from_post($post_id) {
	$product_id = get_post_meta( $post_id, 'ew-product_lock', true );

	if ($product_id) {
		return wc_get_product($product_id);
	} 
	return false;
}

/**
 * Check if customer bought product
 */
function ew_customer_has_access ( \WP_User $user, $post_id ) {
	$posts = get_user_meta($user->ID, 'customer_has_access', true);
	if (is_array($posts) && in_array($post_id, $posts)) {
		return true;
	}
	return false;
}

/**
 * Give customer access to post
 */
function ew_grant_customer_access ( \WP_User $user, $post_id) {
	$posts = get_user_meta($user->ID, 'customer_has_access', true);
	if (!is_array($posts)) {
		$posts = array();
	}
	$posts[] = $post_id;

	update_user_meta( $user->ID, 'customer_has_access', $posts );
}


/**
 * Redirect to product page after order complete
 */
function wc_redirect_after_order_completion( $order_id ) {
	$order = new WC_Order( $order_id );
	
	// @TODO get content page from post meta by product id of order
	$content_page_id = 10;

	$url = get_permalink($content_page_id);
	
	if ( $order->status == 'completed' ) {
	    wp_redirect($url);
	    exit;
	}
}
add_action( 'woocommerce_thankyou', 'wc_redirect_after_order_completion' );

/**
 * Add access after payment completed
 */
function ew_add_access_after_order_complete( $order_id ){
    $order = new WC_Order( $order_id );
    $user = $order->get_user();
    if ( !in_array('company', $user->roles, true) ) {
    	$items = $order->get_items(); 
    	foreach ( $items as $item ) {
    	    $item_id = $item['product_id'];
    	    $product = wc_get_product($item_id);
					if ($post = ew_get_associated_post_from_product($product->get_id()) ) {
			    	ew_grant_customer_access( $user, $post->ID );
					}
    	}
    }
}
add_action( 'woocommerce_order_status_completed', 'ew_add_access_after_order_complete', 10, 1 );

<?php

/**
 * Add user roles on theme activation
 */
function ew_add_user_roles() {
       add_role( 'company', 'Company', array( 'read' => true, 'level_0' => true ) );
       add_role( 'employee', 'Employee', array( 'read' => true, 'level_0' => true ) );
   }
add_action( 'after_switch_theme', 'ew_add_user_roles' );

/**
 * Get employees from company user ID
 */
function ew_get_employees(\WP_User $company) {
  if ( ! in_array('company', $company->roles, true) ) {
    return array();
  }

  $meta = get_user_meta($company->ID, 'company_employees', true);
  if (empty($meta)) {
    return array();
  }

  $query = new WP_User_Query(array(
    'role'    => 'employee',
    'include' => (array) $meta
  ));

  return $query->results;
}

/**
 * Get user company from user object
 */
function ew_get_user_company_name(\WP_User $user) {
  if ( in_array('employee', $user->roles, true) ) {
    $company = ew_get_company($user);
    return get_user_meta($company->ID, 'company', true);
  }

  return get_user_meta($user->ID, 'company', true);
}

/**
 * Get company from employee user ID
 */
function ew_get_company(\WP_User $employee) {

  if ( in_array('company', $employee->roles, true) ) {
    return $employee;
  }

  if ( ! in_array('employee', $employee->roles, true) ) {
    return;
  }

  $company = get_users(
    array(
      'number' => 1,
      'role' => 'company',
      'meta_query' => array(
        array(
            'key' => 'company_employees',
            'value' => serialize( $employee->ID ),
            'compare' => 'LIKE'
        ),
      )
    )
  );

  if (count($company)) {
    return $company[0];
  }
  else return;
}


/**
 * Add a user to a company via company user
 */
function ew_assign_user_to_company(\WP_User $employee, \WP_User $company) {

  if ( ! in_array('company', $company->roles, true) ) {
     return false;
  }

  if ( ! in_array('employee', $employee->roles, true) ) {
     return false;
  }

  $employees = get_user_meta($company->ID, 'company_employees', true);
  if (empty($employees)) {
    $employees = array();
  } 

  $employees[] = $employee->ID;
  $update = update_user_meta($company->ID, 'company_employees', $employees);

  return (int) $update > 0;
}

/**
 * Remove a user from a company via company user
 */
function ew_remove_user_from_company(\WP_User $employee, \WP_User $company) {

  if ( ! in_array('company', $company->roles, true) ) {
     return false;
  }

  if ( ! in_array('employee', $employee->roles, true) ) {
     return false;
  }

  $employees = get_user_meta($company->ID, 'company_employees', true);
  if (empty($employees) || !is_array($employees)) {
    return;
  } 

  if(($key = array_search($employee->ID, $employees)) !== false) {
      unset($employees[$key]);
  }
  $update_company = update_user_meta($company->ID, 'company_employees', $employees);
  $update_employee = wp_update_user( array( 'ID' => $employee->ID, 'role' => 'customer' ) );

  return (int) $update_company > 0 && (int) $update_employee > 0;
}

/**
 * Validate unique email
 */
function ew_check_if_employee_email_taken( $result, $tag ) {
  if ( 'employee-email' == $tag->name ) {
    $email = isset( $_POST['employee-email'] ) ? trim( $_POST['employee-email'] ) : '';
    if ( email_exists($email) ) {
        $result->invalidate( $tag, "This email has already been registered." );
    }
  }
  return $result;
}
add_filter( 'wpcf7_validate_email*', 'ew_check_if_employee_email_taken', 20, 2 );

/**
 * Employee registration
 */
function ew_register_employee($cfdata) {
  if (!isset($cfdata->posted_data) && class_exists('WPCF7_Submission')) {
    $submission = WPCF7_Submission::get_instance();
    if ($submission) {
      $formdata = $submission->get_posted_data();
    }
  } elseif (isset($cfdata->posted_data)) {
    $formdata = $cfdata->posted_data;
  } else {
    return $cfdata;
  }
  // Check this is the user registration form
  if ( $cfdata->title() == 'Employee Registration') {
      $password = wp_generate_password( 12, false );
      $email = $formdata['employee-email'];
      $name = $formdata['employee-name'];
      $current_user_id = $formdata['cuid'];
      $username = strtolower(str_replace(' ', '', $name));
      $name_parts = explode(' ',$name);

      if ( !email_exists( $email ) ) {
          // Find an unused username
          $username_available = $username;
          $i = 1;
          while ( username_exists( $username_available ) ) {
              $username_available = $username . $i++;
          }
          $username = $username_available;
          $userdata = array(
              'user_login' => $username,
              'user_pass' => $password,
              'user_email' => $email,
              'nickname' => reset($name_parts),
              'display_name' => $name,
              'first_name' => reset($name_parts),
              'last_name' => end($name_parts),
              'role' => 'employee',
          );
          $user_id = wp_insert_user( $userdata );
          if ( !is_wp_error($user_id) ) {
            $employee = get_user_by('id', $user_id);
            $company = get_user_by('id', $current_user_id);
            // Assign user to employee
            ew_assign_user_to_company($employee, $company);
            // Email login details to user
            $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
            $message = "An account for you has been created at Edgedweller. Your login details are as follows:" . "\r\n";
            $message .= sprintf(__('Username: %s'), $username) . "\r\n";
            $message .= sprintf(__('Password: %s'), $password) . "\r\n";
            $message .= wp_login_url() . "\r\n";
            wp_mail($email, sprintf(__('[%s] Your username and password'), $blogname), $message);
          }
      }
  }
  return $cfdata;
}
add_action('wpcf7_before_send_mail', 'ew_register_employee', 1);

/**
 * Shortcode for dynamic use in CF7
 */
function ew_current_user_id(){
  return get_current_user_id();
}
add_shortcode('EW_CURRENT_USER_ID', 'ew_current_user_id');

/**
 * Registration fields for Woocommerce
 */
function ew_wc_add_fields_to_registration_start() {?>
  <p class="form-row form-row-first">
    <label for="reg_billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
  </p>
  <p class="form-row form-row-last">
    <label for="reg_billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
  </p>
  <?php
 }
add_action( 'woocommerce_register_form_start', 'ew_wc_add_fields_to_registration_start' );

/**
 * Registration fields for Woocommerce
 */
function ew_wc_add_fields_to_registration() {?>
  <p class="form-row form-row-first">
    <label for="reg_company"><?php _e( 'Company', 'woocommerce' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="company" id="reg_company" value="<?php if ( ! empty( $_POST['company'] ) ) esc_attr_e( $_POST['company'] ); ?>" />
  </p>
  <p class="form-row form-row-last">
    <label for="reg_job_title"><?php _e( 'Job Title', 'woocommerce' ); ?></label>
    <input type="text" class="input-text" name="job_title" id="reg_job_title" value="<?php if ( ! empty( $_POST['job_title'] ) ) esc_attr_e( $_POST['job_title'] ); ?>" />
  </p>
  <?php
 }
add_action( 'woocommerce_register_form', 'ew_wc_add_fields_to_registration' );

/**
 * Validate WC registration fields
 */
function ew_wc_validate_registration_fields( $username, $email, $validation_errors ) {
 
  if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
    $validation_errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: First name is required', 'woocommerce' ) );
  }

  if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
    $validation_errors->add( 'billing_last_name_error', __( '<strong>Error</strong>: Last name is required', 'woocommerce' ) );
  }

  if ( isset( $_POST['company'] ) && empty( $_POST['company'] ) ) {
    $validation_errors->add( 'company_error', __( '<strong>Error</strong>: Company name is required', 'woocommerce' ) );
  }
  return $validation_errors;
}
 
add_action( 'woocommerce_register_post', 'ew_wc_validate_registration_fields', 10, 3 );


/**
* Save WC registration fields
*/
function ew_wc_save_registration_fields( $customer_id ) {
  if ( isset( $_POST['billing_first_name'] ) ) {
    update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
    update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
  }
  if ( isset( $_POST['billing_last_name'] ) ) {
    update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
    update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
  }
  if ( isset( $_POST['company'] ) ) {
    update_user_meta( $customer_id, 'company', sanitize_text_field( $_POST['company'] ) );
  }
  if ( isset( $_POST['job_title'] ) ) {
    update_user_meta( $customer_id, 'job_title', sanitize_text_field( $_POST['job_title'] ) );
  }
}
add_action( 'woocommerce_created_customer', 'ew_wc_save_registration_fields' );

/**
 * Disable pay later for all but companies
 */
function ew_restrict_pay_later( $available_gateways ) {
  global $woocommerce;
  $user = wp_get_current_user();
  if ( in_array('company', $user->roles, true) || in_array('employee', $user->roles, true) ) {
    // All payments available
  } else {
    unset( $available_gateways['pay_later'] );
  }
  return $available_gateways;
}
add_filter( 'woocommerce_available_payment_gateways', 'ew_restrict_pay_later' );
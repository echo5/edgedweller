<?php

/**
 * Enqueue scripts and styles.
 */
function ew_assessment_scripts() {
	if ( is_singular('wpt_test') ) {
	wp_enqueue_script( 'ew-assessments', get_stylesheet_directory_uri() . '/assets/js/assessments.js', array('jquery'), '', true );
	}
}
add_action( 'wp_enqueue_scripts', 'ew_assessment_scripts', 110 );

/**
 * Shortcode for help tooltip
 */
function ew_help_shortcode($atts, $content = ''){
	$atts = shortcode_atts( array(
	), $atts, 'help' );

	$output = '<div class="help tooltip">';
	$output .= '<i class="fa fa-question"></i>';
	$output .= '<div class="tooltip-text"><div class="tooltip-inner">' . $content . '</div></div>';
	$output .= '</div>';
	echo $output;
}
add_shortcode('help', 'ew_help_shortcode');

/**
 * Redirect to test results if already taken
 */
function ew_check_if_assessment_results() {

	if (is_singular('wpt_test')) {
		global $wpdb;

		$current_user = wp_get_current_user();
		$test_id = get_the_ID();
		$passings_table = $wpdb->prefix . "t_passings";
		$query = $wpdb->prepare("SELECT * FROM $passings_table WHERE respondent_id = $current_user->ID AND test_id = %d", $test_id);
		$passings = $wpdb->get_results($query);

		if ( count($passings) ) {
			$passingModel = new WpTesting_Model_Passing($passings[0]->passing_id);
			$passing_url = $passingModel->getUrl();
			global $wp;
			$current_url = home_url(add_query_arg(array(),$wp->request));
			if (trailingslashit($current_url) != $passing_url){
				wp_redirect($passing_url);
				exit;
			}
		}
	}

}
add_action('template_redirect', 'ew_check_if_assessment_results');
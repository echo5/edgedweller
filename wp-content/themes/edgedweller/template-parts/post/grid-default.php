<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="featured mb-3">
		<?php the_post_thumbnail($GLOBALS['image_size']); ?>
		<a href="<?php the_permalink(); ?>" class="overlay-link"></a>
	</div><!-- .featured -->
	
	<div class="post-info">
		<div class="categories mb-3">
			<?php the_category(', '); ?>
		</div>
		<a href="<?php echo the_permalink(); ?>">
			<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>
		</a>
		<div class="post-meta mt-3">
			<span class="author"><?php the_author(); ?></span>
			<span class="date"><?php the_date(fusion_default_date_format()); ?></span>
		</div>
	</div>


</article><!-- #post-## -->
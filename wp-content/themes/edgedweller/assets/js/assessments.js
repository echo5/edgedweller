(function($){

	$('.wpt_test>.content:first-child').append('<div class="selected-answers-wrapper"><div id="selected-answers" class="selected-answers"></div></div>');
	var selectedAnswers = $('#selected-answers');
	var totalSelected = 0;

	/**
	 * Set all to 'no' by default
	 */
  $('.question').each(function() {
  	$(this).find('.answer:first input').prop('checked', true);
  });

  /**
   * Selecting answer
   */
	$('.question>.title').click(function() {
		if (totalSelected < 10) {
				$(this).siblings('.answer:nth-child(3)').find('input').prop('checked', true);
				var id = 'question-' + parseInt($(this).find('.number').text());
				var text = $(this).find('.title').text();
			 	selectedAnswers.append('<div class="selected-answer" data-question-id="' + id + '">' + text + '</div>');
				$(this).parent().attr('id', id).hide();
			 	$( document ).trigger( "answers_changed" );
		 } else {
		 	showNotification('Too many items selected, please limit to 10 answers total.');
		 }
	});

	 /**
	  * Hide/show header content on answer change
	  */
	 $(document).on('answers_changed', function() {
	 	var headerContent = $('.wpt_test > .content:first-child').find('h1, p');
	 	if (selectedAnswers.children().length > 0) {
	 		headerContent.hide();
	 	} else {
	 		headerContent.show();
	 	}
	 });

	 /**
	  * Update total selected
	  */
	 $(document).on('answers_changed', function() {
	 	 totalSelected = selectedAnswers.children().length;
	 });

	/**
	 * Removing answers
	 */
	 $(document).on('click', '.selected-answer', function() {
	  $('#' + $(this).data('question-id')).show().find('.answer:nth-child(2) input').prop('checked', true);
	  $(this).remove();
	 	$( document ).trigger( "answers_changed" );
	 });

	/**
	 * Validation
	 */
	console.log($('.wpt_test_form').attr('data-questions'));

	/**
	 * Show modal notification
	 */
	function showNotification(text) {
		var modal = $('#notification-modal');
		if (!modal.length) {
			html = '<div class="modal" id="notification-modal" style="display: block;">' +
			  '<div class="modal-sandbox"></div>' +
			  '<div class="modal-box">' +
			    '<div class="modal-header">' +
			      '<div class="close-modal">&#10006;</div>' +
			    '</div>' +
			    '<div class="modal-body">' +
			    	text +
			    '</div>' +
			    '<div class="modal-footer">' +
			      '<button class="close-modal">Close</button>' +
		      '</div>' +
			  '</div>' +
			'</div>';
			modal = $('html').append(html);
			modal.show();
		} else {
			modal.find('.modal-body').text(text);
			modal.show();
		}
	}

	/** 
	 * Show modal on click
	 */
	$(".modal-trigger").click(function(e){
	  e.preventDefault();
	  dataModal = $(this).attr("data-modal");
	  $("#" + dataModal).css({"display":"block"});
	});

	/**
	 * Hide modal on click
	 */
	$(document).on('click', '.close-modal, .modal-sandbox', function(){
	  $(".modal").css({"display":"none"});
	});

	/**
	 * Hide modal on click
	 */
	$('.wpt_test_form').submit(function(e) {
		if (totalSelected == 10) {
			return true;
		} else {
		 	showNotification('Please select 10 items before submitting.');
			return false;
		}
	});

})(jQuery);
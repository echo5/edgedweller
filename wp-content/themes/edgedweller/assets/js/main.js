(function($) {

	$(window).scroll(function() {    
	    var scroll = $(window).scrollTop();

	    if (scroll >= 300) {
	        $(".navbar.fixed-top").addClass("is-scrolled");
	    } else {
	        $(".navbar.fixed-top").removeClass("is-scrolled");
	    }
	});

})(jQuery);
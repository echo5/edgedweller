<?php

class Luxe_Portfolio {

	/**
	 * Register the post type and taxonomy
	 *
	 * @uses admin_hooks
	 * @author Matthew Boynes
	 */
	function __construct() {
			$this->register_post_type();
			$this->register_taxonomy();
	}

	function register_post_type() {
			$labels = array(
				'name'               => esc_attr__( 'Portfolio', 'luxe-post-types' ),
				'singular_name'      => esc_attr__( 'Portfolio Item', 'luxe-post-types' ),
				'menu_name'          => esc_attr__( 'Portfolio', 'luxe-post-types' ),
				'name_admin_bar'     => esc_attr__( 'Portfolio', 'luxe-post-types' ),
				'add_new'            => esc_attr__( 'New Portfolio Item', 'luxe-post-types' ),
				'add_new_item'       => esc_attr__( 'New Portfolio Item', 'luxe-post-types' ),
				'new_item'           => esc_attr__( 'New Portfolio Item', 'luxe-post-types' ),
				'edit_item'          => esc_attr__( 'Edit Portfolio Item', 'luxe-post-types' ),
				'view_item'          => esc_attr__( 'View Portfolio Item', 'luxe-post-types' ),
				'all_items'          => esc_attr__( 'All Portfolio Items', 'luxe-post-types' ),
				'search_items'       => esc_attr__( 'Search Portfolio', 'luxe-post-types' ),
				'parent_item_colon'  => esc_attr__( 'Parent Portfolio:', 'luxe-post-types' ),
				'not_found'          => esc_attr__( 'No portfolio items found.', 'luxe-post-types' ),
				'not_found_in_trash' => esc_attr__( 'No portfolio items found in Trash.', 'luxe-post-types' )
			);

			$args = array(
				'labels'             => $labels,
		        'description'        => esc_attr__( 'Organize and show off your work publicly by adding portfolio items.', 'luxe-post-types' ),
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'query_var'          => true,
				'rewrite'            => array( 'slug' => get_theme_mod( 'portfolio_slug', 'portfolio' ) ),
				'capability_type'    => 'post',
				'has_archive'        => false,
				'hierarchical'       => false,
				'menu_position'      => null,
				'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'post-formats', 'page-attributes' ),
			);
			register_post_type( 'portfolio', $args );
	}

	function register_taxonomy() {
		register_taxonomy( 'portfolio_category', array('portfolio'), array(
		    'hierarchical' => true, 
		    'label' => 'Categories', 
		    'singular_label' => 'Category', 
		    'rewrite' => array( 'slug' => 'category', 'with_front'=> false )
		    )
		);
		register_taxonomy_for_object_type( 'portfolio_category', 'portfolio' );
	}

}
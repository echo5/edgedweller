<?php
/**
 * ThemeLuxe Post Types
 *
 * @package   Luxe_Post_Types
 * @author    ThemeLuxe
 * @license   GPL-2.0+
 * @link      http://themeluxe.com
 *
 * @wordpress-plugin
 * Plugin Name: ThemeLuxe Post Types
 * Plugin URI:  http://themeluxe.com
 * Description: Creates a portfolio post type and taxonomies.
 * Version:     1.0.0
 * Author:      ThemeLuxe
 * Author URI:  http://www.themeluxe.com/
 * Text Domain: luxe-post-types
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

require_once plugin_dir_path( __FILE__ ) . 'inc/luxe-portfolio.php';

class Luxe_Post_Types {

	/**
	 * Initialize the plugin and call the appropriate hook method
	 *
	 * @uses admin_hooks
	 * @author Matthew Boynes
	 */
	function __construct() {
		add_action( 'init', array( $this, 'init_post_types' ) );
	}

	function init_post_types() {
		new Luxe_Portfolio();
	}

}

$luxe_post_types = new Luxe_Post_Types();